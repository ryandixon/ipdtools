﻿using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace IPD.WebAPI.Filters
{
    
    public class UsersDbContext : IdentityDbContext<Models.ApplicationUser>
    {
        static UsersDbContext()
        {
            Database.SetInitializer(new Initializer());
        }

        private class Initializer : CreateDatabaseIfNotExists<UsersDbContext>
        {
            protected override void Seed(UsersDbContext context)
            {
                IdentityRole role = context.Roles.Add(new IdentityRole("User"));

                var user = new Models.ApplicationUser("devUser");
                user.Roles.Add(new IdentityUserRole { RoleId = role.Id });
                user.Claims.Add(new IdentityUserClaim
                {
                    ClaimType = "hasRegistered",
                    ClaimValue = "true"
                });

                user.PasswordHash = new PasswordHasher().HashPassword("giddyup");
                context.Users.Add(user);
                context.SaveChanges();
                base.Seed(context);
            }
        }
    }
}