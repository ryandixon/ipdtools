﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;

namespace IPD.WebAPI.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser() { }

        public ApplicationUser(string userName)
            : base(userName)
        {

        }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }

        public int BusinessEntityId { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            //: base("DefaultConnection", throwIfV1Schema: false)
            : base("ApiDbContext", throwIfV1Schema: false)            
        {
        }
        
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }

    public class ApplicationDbContextInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            IdentityRole role = context.Roles.Add(new IdentityRole("User"));

            try
            {
                var user = new Models.ApplicationUser("devUser")
                {
                    Email = "rdixon@datatrac.com",
                    BusinessEntityId = 40000,
                    SecurityStamp="thisInitialValueDoesNotMatter",
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = false,
                };
                //user.Roles.Add(new IdentityUserRole { RoleId = role.Id });
                //user.Claims.Add(new IdentityUserClaim
                //{
                //    ClaimType = "hasRegistered",
                //    ClaimValue = "true"
                //});

                user.PasswordHash = new PasswordHasher().HashPassword("giddyup");
                context.Users.Add(user);
                context.SaveChanges();
            }
            catch (Exception e)
            { 
                throw;
            }
            base.Seed(context);
        }
    }
}