﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using NewCo.Models.JSON;
namespace IPD.WebAPI.Controllers
{
    public class OrderController : ApiController
    {
        /// <summary>
        /// Retrieves a specific order based on the order number
        /// </summary>
        /// <param name="orderNumber"></param>
        /// <returns>Order JSON</returns>
        [Route("api/order/{orderNumber}")]
        [HttpGet]        
        public IHttpActionResult Get(string orderNumber)
        {
            //AgentOrder order = new AgentOrder()
            //{
            //    Id = Guid.NewGuid(),
            //    OrderNumber = "00123456",                 
            //};
            //order.Stops.Add(new Stop()
            //{
            //    Id = Guid.NewGuid(),
            //    ContactNumber = "30396055",
            //    ContactName = "Fred"
            //});

            return Ok(null);
        }

        public HttpResponseMessage PostOrder(AgentOrder order)
        {

            return Ok(null);
        }
    }
}
