﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using Autofac;
using NewCo.Data.Relational;
using NewCo.Data.Sequences;
using IPD.WebAPI.Models;

namespace IPD.WebAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        public static IContainer Container { get; set; }

        protected void Application_Start()
        {

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            Database.SetInitializer(new ApplicationDbContextInitializer());
            using (var appContext = new ApplicationDbContext())
            {
                appContext.Database.Initialize(true);
            }
            

            Database.SetInitializer(new NewCo.Data.Relational.NewCoRelationalInitializer());
            using (NewCoRelationalContext db = new NewCoRelationalContext())
            {
                db.Database.Initialize(false);
            }

            Database.SetInitializer(new NewCo.Data.Sequences.SequenceContextInitializer());
            using (SequenceContext db = new SequenceContext())
            {
                db.Database.Initialize(false);
            }

        }
    }
}
