﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Models.JSON
{
    public class StopLocation
    {
        public Guid Id { get; set; }

        public Address Address { get; set; }
        
        public string Note { get; set; }
    }
}
