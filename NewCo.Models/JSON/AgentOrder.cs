﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Models.JSON
{
    public class AgentOrder: IOrder
    {
        public Guid? id { get; set; }

        public string? OrderNumber { get; set; }

        /*
         * Since a customer is likely a reusable resource, we'll require clients of the API to first create a customer BE and reference that entity here.
         */
        /// <summary>
        /// NewCo's identifier for the BusinessEntity associated with the customer
        /// </summary>
        public int CustomerNumber { get; set; }

        /// <summary>
        /// The Agent's identifier for the BusinessEntity associated with the customer
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// The Service Provider (Driver) that will complete the order
        /// </summary>
        public int? ServiceProviderNumber { get; set; }

        public decimal? GrossSettlementAmount { get; set; }

        public DateTime CompletionDateTime { get; set; }


        public IEnumerable<Stop> Stops { get; set; }

    }
    public interface IOrder { }
}
