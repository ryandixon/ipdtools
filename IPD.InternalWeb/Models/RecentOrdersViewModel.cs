﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using IPD.Data.Mongo;

namespace IPD.InternalWeb.Models
{
    public class RecentOrdersViewModel
    {
        private readonly DateTime creationDateTime;
        public RecentOrdersViewModel() { creationDateTime = DateTime.Now; }

        public DateTime CreationDateTime { get { return creationDateTime; } }
        public List<Order> RecentOrders { get; set;}
    }
}