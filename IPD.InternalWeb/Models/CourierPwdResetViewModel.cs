﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPD.InternalWeb.Models
{
    public class CourierPwdResetViewModel
    {
        public string SecurityToken { get; set; }
        public List<CourierPwdResetItem> ResetItems { get; set; }
        public string Message { get; set; }

        //public List<Tuple<string, string, string>> ResetTuples { get; set; }
    }

    public class CourierPwdResetItem
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Result { get; set; }
    }
}