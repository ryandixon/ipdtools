﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IPD.InternalWeb.Models
{
    public class DriverProfileModel: JSONBase
    {
        public string id { get; set; }

        public string Status { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Mobile Number")]
        public string MobileNumber { get; set; }

        [Display(Name="Street Address 1")]
        public string StreetAddress1 { get; set; }
        [Display(Name = "Street Address 2")]
        public string StreetAddress2 { get; set; }        
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }

        
        [Display(Name = "The payee Id at the bank")]
        public string ACHPayeeId { get; set; }

        [Display(Name = "Preferred Payment Method")]
        public IPD.Data.Oracle.PaymentMethod PreferredPmtMethod { get; set; }
        [Display(Name = "Approved For Payment")]
        public bool ApprovedForPmt { get; set; }

        public string BadgeId { get; set; }
         
    }

    
}