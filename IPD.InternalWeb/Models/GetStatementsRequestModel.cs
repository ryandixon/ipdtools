﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IPD.InternalWeb.Models
{
    public class GetStatementsRequestModel
    {
        public Guid BatchId {get; set;}

        [Display(Name = "Message Header", Description = "A title to the message to be included at the bottom of the driver's statement.")]
        public string MessageHeader { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Message", Description = "A message to be included at the bottom of the driver's statement.")]
        public string Message { get; set; }

        [Display(Name = "Report Format")]
        public string Format { get; set; }

        //[Display(Name = "Report Format")]
        public string newfield { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Additional Items", Description = "1 line per item in the format of '<driver number>,<offer amount>, <fee amount>'.")]
        public string AdditionalPayableItems { get; set; }
    }

    //public class GetStatementsRequestModel
    //{
    //    public Guid BatchId {get; set;}

    //    //[Display(Name="Message Header",Description="A title to the message to be included at the bottom of the driver's statement.")]
    //    public string MessageHeader { get; set; }

    //    //[DataType(DataType.MultilineText)]
    //    //[Display(Name = "Message", Description = "A message to be included at the bottom of the driver's statement.")]
    //    public string Message { get; set; }

    //    //[Display(Name = "Report Format")]
    //    public string Format { get; set; }

    //    //[DataType(DataType.MultilineText)]
    //    //[Display(Name = "Additional Items", Description = "1 line per item in the format of '<driver number>,<offer amount>, <fee amount>'.")]
    //    public string AdditionalPayableItems { get; set; }
    //}
}