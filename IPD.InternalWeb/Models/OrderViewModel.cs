﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPD.InternalWeb.Models
{
    public class OrderViewModel
    {
        public string Number { get; set; }
        public string Status { get; set; }
        public string CustomerName { get; set; }
        public string DriverName { get; set; }
        public string DriverMobileNumber { get; set; }
        
    }
}