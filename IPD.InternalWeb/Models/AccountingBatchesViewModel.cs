﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using IPD.Data.Accounting;
using System.ComponentModel.DataAnnotations;
namespace IPD.InternalWeb.Models
{
    public class AccountingBatchesViewModel    
    {
        public AccountingBatchesViewModel() { }
        public AccountingBatchesViewModel(AccountingBatch.AccountingBatchTypes type)
        {
            this.BatchType = type;
            Batches = new List<AccountingBatch>();
            AsOfDate = DateTime.Now;
        }

        public string NewBatchActionName { get; set; }
        public string GetStatementsActionName { get; set; }

        private bool batchTypeSet = false;
        private AccountingBatch.AccountingBatchTypes batchType;
        public AccountingBatch.AccountingBatchTypes BatchType
        {
            get { return batchType; }
            set 
            {
                if (batchTypeSet)
                    throw new Exception("The BatchType has already been set and cannot be changed.");

                batchTypeSet = true;
                batchType = value;
            }
        }

        public List<AccountingBatch> Batches { get; set; }

        public AccountingBatch SelectedBatch { get; set; }
        public AccountingBatch NewBatch { get; set; }
        public string SelectedBatchId { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true, NullDisplayText = "Please select...")]
        [Display(Name = "As Of Date")]
        public DateTime AsOfDate { get; set; }

        public GetStatementsRequestModel GetStatementsRequestModel { get; set; }
    }
}