﻿using IPD.Data.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;


namespace IPD.InternalWeb.Models
{
    public class DriverActivityViewModel
    {
        public DriverActivityViewModel()
        {
            this.HistoryLimit = 200;
            this.HistoryPageNumber = 1;
        }

        private string mobileNumber;
        [Phone]
        public string MobileNumber
        {
            get { return mobileNumber; }
            set
            {
                mobileNumber = System.Text.RegularExpressions.Regex.Replace(value, "[ ()-.]", "");
            }
        }
        
        public IPD.Data.Oracle.DRIVER Driver {get; set;}

        public DriverGps DriverGPS {get; set;} 

        public List<GpsData> GPSDatas {get; set;}

        public List<DriverInMessage> DriverInMessages { get; set; }
        
        [DisplayName("Items Per Page")]
        public int HistoryLimit { get; set; }

        [DisplayName("Page")]
        public int HistoryPageNumber { get; set; }
    }
}