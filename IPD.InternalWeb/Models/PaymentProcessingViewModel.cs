﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using IPD.Reports.DataSetTypes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
namespace IPD.InternalWeb.Models
{
    public class PaymentProcessingViewModel
    {
        public PaymentProcessingViewModel()
        {
            
        }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true, NullDisplayText="Please select...")]
        [Display(Name="As Of Date")]
        public DateTime AsOfDate { get; set; }


        [Display(Name="Message Header",Description="A title to the message to be included at the bottom of the driver's statement.")]
        public string MessageHeader { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Message", Description = "A message to be included at the bottom of the driver's statement.")]
        public string Message { get; set; }

        [Display(Name = "Report Format")]
        public string Format { get; set; }

        public bool HasReport { get; set; }

        public List<DriverStatement> DriverStatements { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Additional Items", Description = "1 line per item in the format of '<driver number>,<offer amount>, <fee amount>'.")]
        public string AdditionalPayableItems { get; set; }

        public List<string> PayableOrderNumbers
        {
            get
            {
                return (from s in DriverStatements
                       from po in s.PayableOrders
                       where po.OrderNumber != String.Empty
                       select po.OrderNumber).ToList();
            }
        }
        public string OracleUpdateStatements
        {
            get
            {
                var orderNumberList = string.Join(",", (from s in PayableOrderNumbers
                                                        select "'" + s + "'"));
                return String.Format("UPDATE ord SET ord_status='PAYMENT_COMPLETE' WHERE ord_number in (\r\n{0}\r\n)", orderNumberList);
            }
        }

        public string OracleUpdateStatementResult { get; set; }

        public string MongoUpdateStatements
        {
            get
            {
                var orderNumberList = string.Join(",", (from s in PayableOrderNumbers
                                                        select "'" + s + "'"));
                return "db.Order.update({strOrdNumber:{$in:[\r\n" + orderNumberList + "\r\n]}},{$set:{strOrdStatus:'PAYMENT_COMPLETE'}},{multi:true})";
            }
        }

        public string MongoUpdateResults { get; set; }

        [Display(Name = "Execute Update Statements", Description = "Run the update statements displayed below")]
        public bool ExecuteUpdates { get; set; }

    }

    
}