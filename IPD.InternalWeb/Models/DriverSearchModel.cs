﻿using IPD.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPD.InternalWeb.Models
{
    public class DriverSearchModel
    {
        public string Term { get; set; }
        public List<DriverSearchResult> Results { get; set; }
    }


}