﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPD.InternalWeb.Models
{
    public class OAccess
    {
        public string SecurityToken { get; set; }
        public string Statement { get; set; }
        public string Message { get; set; }
        public bool IsError { get; set; }
        public object[] result { get; set; }
    }
}