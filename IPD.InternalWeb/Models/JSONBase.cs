﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPD.InternalWeb.Models
{
    public class JSONBase
    {
        public bool success { get; set; }
 
        public bool hasError { get; set; }

        public string errorDescription { get; set; }

        public string errorCode { get; set; }
    }
}