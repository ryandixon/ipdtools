﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPD.InternalWeb.Controllers
{
    public class DriverController : Controller
    {
        // GET: Driver
        public ActionResult Index()
        {
            return View( "~/Views/Driver/DriverProfileView.cshtml");
        }

        [HttpGet]
        public ActionResult Profile(string id)
        {
            
            if (String.IsNullOrEmpty(id))
                return View("~/Views/Driver/DriverProfileView.cshtml");
            else
                return View("~/Views/Driver/DriverProfileView.cshtml", new Models.DriverProfileModel { MobileNumber = id });
        }

        [HttpPost]
        public ActionResult FindDriver(string mobileNumber)
        {
            var model = new Models.DriverProfileModel { MobileNumber = mobileNumber };
            using (var ipdEntities = new IPD.Data.Oracle.Entities())
            {
                var driverService = new IPD.Service.DriverService();
                var driver = driverService.GetDriverInfo(ipdEntities, model.MobileNumber);
                model.success = driver != null;
                if (driver != null)
                {
                    model.Status = driver.DRV_STATUS;
                    model.id = driver.DRV_ID.ToOracleGuid().ToOracleString();
                    model.MobileNumber = driver.DRV_MOBILE_NUMBER;
                    model.FirstName = driver.FirstName;
                    model.LastName = driver.LastName;
                    model.StreetAddress1 = driver.DRV_STREET;
                    model.StreetAddress2 = driver.DRV_SUITE;
                    model.City = driver.DRV_CITY;
                    model.State = driver.DRV_STATE;
                    model.PostalCode = driver.DRV_ZIP;

                    model.ACHPayeeId = driver.ACH_PAYEEID;
                    model.ApprovedForPmt = driver.APPROVED_FOR_PMT == "Y";
                    model.PreferredPmtMethod = driver.PreferredPaymentMethod;
                    model.BadgeId = driver.DRV_BADGEID;
                }
                else
                {
                    model.errorDescription = "Driver not found";
                }
            }
            return Json(model);
        }

        [HttpPost]
        public ActionResult UpdateDriver(Models.DriverProfileModel model)
        {
            try
            {
                model.success = false;
                model.hasError = false;

                using (var ipdEntities = new IPD.Data.Oracle.Entities())
                {
                    var driverService = new IPD.Service.DriverService();
                    var driver = driverService.GetDriverInfo(ipdEntities, model.MobileNumber);
                    driver.FirstName = model.FirstName;
                    driver.LastName = model.LastName;
                    driver.DRV_STREET = model.StreetAddress1;
                    driver.DRV_SUITE = model.StreetAddress2;
                    driver.DRV_CITY = model.City;
                    driver.DRV_STATE = model.State;
                    driver.DRV_ZIP = model.PostalCode;

                    driver.DRV_LAST_UPD_TIME = DateTime.UtcNow;

                    driver.ACH_PAYEEID = model.ACHPayeeId;
                    driver.APPROVED_FOR_PMT = model.ApprovedForPmt ? "Y" : "N";
                    driver.PreferredPaymentMethod = model.PreferredPmtMethod;

                    ipdEntities.SaveChanges();
                    model.success = true;
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ve)
            {
                var errMsg = new System.Text.StringBuilder();
                foreach(var item in ve.EntityValidationErrors)
                {
                    foreach (var subItem in item.ValidationErrors)
                        errMsg.AppendLine(string.Format("{0}: {1}", subItem.PropertyName, subItem.ErrorMessage));
                }
                model.success = false;
                model.errorDescription = errMsg.ToString();
                model.hasError = true;
            }
            catch (Exception e)
            {
                model.success = false;
                model.hasError = true;
                model.errorDescription = e.GetBaseException().Message;
            }


            return Json(model);
        }


    }
}