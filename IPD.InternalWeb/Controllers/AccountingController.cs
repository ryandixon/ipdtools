﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Web.Helpers;
using System.Web.UI.DataVisualization.Charting;

using Microsoft.Reporting.WebForms;
using IPD.Reports.DataGenerators;
using IPD.Service;
using IPD.Data.Accounting;

namespace IPD.InternalWeb.Controllers
{
    public class AccountingController : Controller
    {
        const string createtimePlaceholderText = "--creation time--";
        // GET: Accounting
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CollectionBatches()
        {
            var model = new Models.AccountingBatchesViewModel(AccountingBatch.AccountingBatchTypes.Collection);
            model.NewBatchActionName = "NewCollectionBatch";
            model.GetStatementsActionName = "GetCollectionStatements";
            model.NewBatch = new AccountingBatch() { Description = String.Format("Batch on: {0}", createtimePlaceholderText) };

            var accountingService = new AccountingService();
            using (var accountingContext = new IPDAccounting())
            {
                model.Batches = accountingService.GetAccountingBatches(accountingContext,
                    new AccountingBatch.AccountingBatchTypes[] { AccountingBatch.AccountingBatchTypes.Collection },
                    new AccountingBatch.AccountingBatchStatuses[] { 
                        AccountingBatch.AccountingBatchStatuses.New, 
                        AccountingBatch.AccountingBatchStatuses.Processing, 
                        //AccountingBatch.AccountingBatchStatuses.Canceled, 
                        AccountingBatch.AccountingBatchStatuses.Final,  }
                    ).ToList();
            }
            return View("~/Views/Accounting/AccountingBatches.cshtml", model);
        }


        [HttpGet]
        public ActionResult PaymentBatches()
        {
            var model = new Models.AccountingBatchesViewModel(AccountingBatch.AccountingBatchTypes.Payment);
            model.NewBatchActionName = "NewPaymentBatch";
            model.GetStatementsActionName = "GetPaymentStatements";
            model.NewBatch = new AccountingBatch() { Description = String.Format("Batch on: {0}",createtimePlaceholderText) };

            var accountingService = new AccountingService();
            using (var accountingContext = new IPDAccounting())
            {
                model.Batches = accountingService.GetAccountingBatches(accountingContext, 
                    new  AccountingBatch.AccountingBatchTypes[] {AccountingBatch.AccountingBatchTypes.Payment},
                    new AccountingBatch.AccountingBatchStatuses[] { 
                        AccountingBatch.AccountingBatchStatuses.New, 
                        AccountingBatch.AccountingBatchStatuses.Processing, 
                        //AccountingBatch.AccountingBatchStatuses.Canceled, 
                        AccountingBatch.AccountingBatchStatuses.Final,  }
                    ).ToList();
            }
            return View("~/Views/Accounting/AccountingBatches.cshtml", model);
        }

        [HttpPost]
        public ActionResult PaymentBatches(Models.AccountingBatchesViewModel model, string command)
        {
            switch(command)
            {
                case "Add": return NewPaymentBatch(model); break;
                case "SelectBatch": return SelectBatch(model); break;
                default: return PaymentBatches(); break;
            }
        }

        public ActionResult SelectBatch(Models.AccountingBatchesViewModel model)
        {
            //var model = pModel ?? new Models.AccountingBatchesViewModel(AccountingBatch.AccountingBatchTypes.Payment);
            var accountingService = new AccountingService();
            using (var accountingContext = new IPDAccounting())
            {
                model.Batches = accountingService.GetAccountingBatches(accountingContext, new AccountingBatch.AccountingBatchTypes[] { AccountingBatch.AccountingBatchTypes.Payment }).ToList();
                model.SelectedBatch = model.Batches.Where(b => b.id.ToString() == model.SelectedBatchId).FirstOrDefault();
            }
            return View("~/Views/Accounting/AccountingBatches.cshtml", model);

        }

        public JsonResult GetBatch(string pBatchId)
        {
            var accountingService = new AccountingService();
            using (var accountingContext = new IPDAccounting())
            {
                var batchId = new Guid(pBatchId);
                var rtn = (from ab in accountingContext.AccountingBatches
                            where ab.id == batchId
                            select new
                            {
                                batch = ab,
                                OrderCount = ab.OrderAccountingBatches.Count,
                                NextStep = "NextStep"                                
                            })
                            .FirstOrDefault();


                var nextStatus = AccountingBatch.GetNextStatus(rtn.batch.StatusString).ToString();
                var preventCancelStatus = from s in new AccountingBatch.AccountingBatchStatuses[] { AccountingBatch.AccountingBatchStatuses.Final, AccountingBatch.AccountingBatchStatuses.Canceled }
                                          select s.ToString();
                return Json(new
                {
                    rtn.batch.id,
                    Status = rtn.batch.StatusString,
                    rtn.batch.Description,
                    rtn.batch.UpdatedBy,
                    rtn.batch.CreatedDateTime,
                    rtn.batch.MessageHeader,
                    rtn.batch.Message,
                    rtn.OrderCount,
                    result = "success",
                    NextStep = nextStatus == rtn.batch.StatusString ? null : nextStatus,
                    AllowCancel = !preventCancelStatus.Contains(rtn.batch.StatusString)
                });
            }
        }

        public ActionResult NewPaymentBatch(Models.AccountingBatchesViewModel model)
        {
            if (model.BatchType != AccountingBatch.AccountingBatchTypes.Payment)
                throw new Exception("Invalid accounting type");

            NewAccountingBatch(model);

            return PaymentBatches();
        }

        public ActionResult NewCollectionBatch(Models.AccountingBatchesViewModel model)
        {
            if (model.BatchType != AccountingBatch.AccountingBatchTypes.Collection)
                throw new Exception("Invalid accounting type");

            NewAccountingBatch(model);
            return CollectionBatches();
        }
        private AccountingBatch NewAccountingBatch(Models.AccountingBatchesViewModel model)
        {

            var accountingService = new AccountingService();
            AccountingBatch batch = null;
            using (var accountingContext = new IPDAccounting())
            {
                try
                {
                    string description = null;
                    if (model.NewBatch != null)
                    {
                        description = model.NewBatch.Description;
                        if (description != null)
                            description = description.Replace(createtimePlaceholderText, String.Format("{0:dd-MMM-yyyy h:mm tt}", DateTime.Now));
                    }

                    switch(model.BatchType)
                    {
                        case AccountingBatch.AccountingBatchTypes.Collection:
                            batch = accountingService.CreateCollectionBatch(accountingContext, "unknownUser", model.AsOfDate, description);
                            break;
                        case AccountingBatch.AccountingBatchTypes.Payment:
                            batch = accountingService.CreatePaymentBatch(accountingContext, "unknownUser", model.BatchType, model.AsOfDate, description);
                            break;
                    }
                    
                    accountingContext.AccountingBatches.Add(batch);                    
                    accountingContext.SaveChanges();
                    
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException validationException)
                {
                    var msg = validationException.Message;
                    foreach(var error in validationException.EntityValidationErrors)
                    {
                        msg += String.Format("\r\n\r\ntype {0}:", error.Entry.Entity == null ? "null" : error.Entry.Entity.GetType().ToString());
                        foreach(var entry in error.ValidationErrors)
                        {
                            msg += String.Format("\r\n\t{0}: {1}", entry.PropertyName, entry.ErrorMessage);
                        }
                    }

                    System.Diagnostics.Debug.WriteLine(msg);
                    throw;
                }
                catch(Exception e)
                {
                    var innerExc = e;
                    string msg = string.Empty;
                    while (innerExc != null)
                    {
                        msg += String.Format("\r\n{0}", innerExc.Message);
                        innerExc = innerExc.InnerException;
                    }
                    System.Diagnostics.Debug.WriteLine(msg);
                    throw;
                }
            }

            return batch;
        }

        public JsonResult AdvanceBatchStatus(AccountingBatch model)
        {
            var accountingService = new AccountingService();
            AccountingBatch.AccountingBatchStatuses newBatchStatus = AccountingBatch.AccountingBatchStatuses.New;
            if (model != null)
            {
                using (var accountingContext = new IPDAccounting())
                {
                    var accountingBatch = accountingService.AdvanceBatchStatus(accountingContext, null, model.id);
                    newBatchStatus = accountingBatch.Status;
                }
            }
            return GetBatch(model.id.ToString());
        }

        public JsonResult CancelBatch(AccountingBatch model)
        {
            var accountingService = new AccountingService();
            if (model != null)
            {
                using (var accountingContext = new IPDAccounting())
                {
                    var accountingBatch = accountingService.CancelBatch(accountingContext, model.id);
                    accountingContext.SaveChanges();
                }
            }
            return GetBatch(model.id.ToString());
        }

        public JsonResult UpdateBatch(AccountingBatch model)
        {
            var accountingService = new AccountingService();
            if (model != null)
            {
                using (var accountingContext = new IPDAccounting())
                {
                    var accountingBatch = accountingContext.AccountingBatches.Find(model.id);
                    accountingBatch.Description = model.Description;
                    accountingBatch.UpdatedBy = "unknownUser";
                    accountingContext.SaveChanges();
                }
            }
            return Json(new { id=model.id, result = "success" });
        }

        public JsonResult DeleteBatch(AccountingBatch model)
        {
            var result = false;
            using(var accountingContext = new IPDAccounting())
            {
                var service = new AccountingService();
                result = service.DeleteBatch(accountingContext, model.id);
                accountingContext.SaveChanges();
            }

            return Json(new { model.id, result });
        }

        public JsonResult BatchOrderList(AccountingBatch model)
        {
            var accountingService = new AccountingService();
            if (model != null)
            {
                using (var accountingContext = new IPDAccounting())
                {
                    model.OrderAccountingBatches = accountingService.GetBatchOrders(accountingContext, model.id).ToList();
                }
            }

            var results = from oab in model.OrderAccountingBatches
                          select new {
                              oab.OrderId,
                              Status = oab.Status.ToString(),
                              oab.OrderNumber,
                              TotalDue = String.Format("{0:c}", oab.TotalDue),
                              DriverName = oab.DriverName.Replace(","," "),
                              DriverMobileNumber = oab.DriverMobileNumber,
                              IsApprovedForPmt = oab.IsApprovedForPmt,
                              CustomerName = oab.CustomerName,
                              oab.UpdatedBy
                          };
            return Json(results);
        }

        [HttpGet]
        public ActionResult PaymentProcessing()
        {
            ViewBag.Message = "Order History page.";
            var model = new Models.PaymentProcessingViewModel();
            model.AsOfDate = DateTime.UtcNow;
            return View("~/Views/Home/PaymentProcessing.cshtml", model);
        }

        [HttpPost]
        public ActionResult GetPaymentStatements(Models.GetStatementsRequestModel model)
        {
            //return PaymentBatches();
            //var model = pModel.GetStatementsRequestModel;
            var settlementGenerator = new IPD.Reports.DataGenerators.Settlement();
            List<IPD.Reports.DataSetTypes.DriverStatement> statements;
            using (var accountingEntities = new Data.Accounting.IPDAccounting())
            {
                List<IPD.Reports.DataSetTypes.PayableOrder> additionalItems = null;
                if (!String.IsNullOrEmpty(model.AdditionalPayableItems))
                {
                    additionalItems = new List<IPD.Reports.DataSetTypes.PayableOrder>();
                    var lines = model.AdditionalPayableItems.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var line in lines)
                    {
                        var lineText = line;
                        var idxComment = lineText.IndexOf("--");
                        if (idxComment > 0)
                            //this is a comment from position to end of line, get rid of it
                            lineText = lineText.Substring(0, idxComment).Trim();

                        var vals = lineText.Split(",".ToCharArray());
                        decimal offerAmount = 0;
                        decimal.TryParse(vals[3], out offerAmount);
                        decimal feeAmount = 0;
                        decimal.TryParse(vals[4], out feeAmount);

                        additionalItems.Add(new IPD.Reports.DataSetTypes.PayableOrder()
                        {
                            DriverNumber = vals[0],
                            OrderNumber = vals[1],
                            ShipperName = vals[2],
                            OfferAmount = offerAmount,
                            FeeAmount = feeAmount,
                            NetPayableAmount = offerAmount - feeAmount
                        });
                    }
                }

                
                var accountingBatch = accountingEntities.AccountingBatches.Where(ab => ab.id == model.BatchId).First();
                if (accountingBatch.Status != AccountingBatch.AccountingBatchStatuses.Final)
                {
                    accountingBatch.Message = model.Message;
                    accountingBatch.MessageHeader = model.MessageHeader;
                    accountingEntities.SaveChanges();
                }

                statements = settlementGenerator.GenerateDriverStatements(accountingEntities, accountingBatch, additionalItems);
            }

            if (model.Format.ToLower() == "text")
            {
                var sb = new System.Text.StringBuilder();
                foreach(var statement in statements)
                {
                    sb.AppendLine(statement.ToCSV());
                }

                Response.AddHeader("content-disposition", "attachment; filename=statements-" + DateTime.Now.ToString("yyyy-MMM-dd HH:mm:ss") + ".csv");
                return File(System.Text.UTF8Encoding.UTF8.GetBytes(sb.ToString()), "text/plain");
            }

            //model.HasReport = true;
            LocalReport localReport = new LocalReport();
            var dllPath = System.IO.Path.Combine(Server.MapPath(@"/bin"), "IPD.Reports.dll");
            var assembly = System.Reflection.Assembly.LoadFrom(dllPath);
            //localReport.LoadReportDefinition(assembly.GetManifestResourceStream("IPD.Reports.Reports.DriverStatement.rdlc"));
            localReport.LoadReportDefinition(assembly.GetManifestResourceStream("IPD.Reports.Reports.DriverStatementMany.rdlc"));
            localReport.LoadSubreportDefinition("DriverStatement", assembly.GetManifestResourceStream("IPD.Reports.Reports.DriverStatement.rdlc"));
            localReport.LoadSubreportDefinition("StatementPayableOrder", assembly.GetManifestResourceStream("IPD.Reports.Reports.StatementPayableOrder.rdlc"));

            if (model.MessageHeader != null || model.Message != null)
            {
                var parms = new ReportParameterCollection();
                parms.Add(new ReportParameter("Message", model.Message));
                parms.Add(new ReportParameter("MessageHeader", model.MessageHeader));
                localReport.SetParameters(parms);
            }
            localReport.DataSources.Add(new ReportDataSource("DriverStatements", statements));


            localReport.SubreportProcessing += (o, e) =>
            {
                var statement = statements.Where(s => s.DriverId == new Guid(e.Parameters["DriverId"].Values[0].ToString())).FirstOrDefault();
                switch (e.ReportPath)
                {
                    case "DriverStatement":
                        e.DataSources.Add(new ReportDataSource("DriverStatement", new List<IPD.Reports.DataSetTypes.DriverStatement>() { statement }));
                        break;
                    case "StatementPayableOrder":
                        e.DataSources.Add(new ReportDataSource("PayableOrders", statement.PayableOrders));
                        break;
                }
            };

            string reportType = "pdf";
            string mimeType;
            string encoding;
            string fileNameExtension;
            string deviceInfo = string.Empty;

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;
            //Render the report            
            renderedBytes = localReport.Render(reportType, deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);

            Response.AddHeader("content-disposition", "attachment; filename=statements-" + DateTime.Now.ToString("yyyy-MMM-dd HH:mm:ss") + "." + fileNameExtension);
            if (model.Format == null)
                return File(renderedBytes, "image/jpeg");
            else if (model.Format.ToUpper() == "PDF")
                return File(renderedBytes, "pdf");
            else
                return File(renderedBytes, "image/jpeg");

        }

        [HttpPost]
        public ActionResult GetCollectionStatements(Models.GetStatementsRequestModel model)
        {
            //return PaymentBatches();
            //var model = pModel.GetStatementsRequestModel;
            var settlementGenerator = new IPD.Reports.DataGenerators.Settlement();
            List<IPD.Reports.DataSetTypes.CustomerStatement> statements;

            using (var accountingEntities = new Data.Accounting.IPDAccounting())
            {
                var accountingBatch = accountingEntities.AccountingBatches.Where(ab => ab.id == model.BatchId).First();
                if (accountingBatch.Status != AccountingBatch.AccountingBatchStatuses.Final)
                {
                    accountingBatch.Message = model.Message;
                    accountingBatch.MessageHeader = model.MessageHeader;
                    accountingEntities.SaveChanges();
                }

                statements = settlementGenerator.GenerateCustomerStatements(accountingEntities, accountingBatch);
            }

            if (model.Format.ToLower() == "text")
            {
                var sb = new System.Text.StringBuilder();
                foreach(var statement in statements)
                {
                    sb.AppendLine(statement.ToCSV());
                }

                Response.AddHeader("content-disposition", "attachment; filename=statements-" + DateTime.Now.ToString("yyyy-MMM-dd HH:mm:ss") + ".csv");
                return File(System.Text.UTF8Encoding.UTF8.GetBytes(sb.ToString()), "text/plain");
            }

            //model.HasReport = true;
            LocalReport localReport = new LocalReport();
            var dllPath = System.IO.Path.Combine(Server.MapPath(@"/bin"), "IPD.Reports.dll");
            var assembly = System.Reflection.Assembly.LoadFrom(dllPath);
            
            localReport.LoadReportDefinition(assembly.GetManifestResourceStream("IPD.Reports.Reports.CustomerStatementMany.rdlc"));
            localReport.LoadSubreportDefinition("CustomerStatement", assembly.GetManifestResourceStream("IPD.Reports.Reports.CustomerStatement.rdlc"));
            localReport.LoadSubreportDefinition("CustomerStatementPayableOrder", assembly.GetManifestResourceStream("IPD.Reports.Reports.CustomerStatementPayableOrder.rdlc"));

            if (model.MessageHeader != null || model.Message != null)
            {
                var parms = new ReportParameterCollection();
                parms.Add(new ReportParameter("Message", model.Message));
                parms.Add(new ReportParameter("MessageHeader", model.MessageHeader));
                localReport.SetParameters(parms);
            }
            localReport.DataSources.Add(new ReportDataSource("CustomerStatements", statements));


            localReport.SubreportProcessing += (o, e) =>
            {
                var statement = statements.Where(s => s.CustomerId == new Guid(e.Parameters["CustomerId"].Values[0].ToString())).FirstOrDefault();
                switch (e.ReportPath)
                {
                    case "CustomerStatement":
                        e.DataSources.Add(new ReportDataSource("CustomerStatement", new List<IPD.Reports.DataSetTypes.CustomerStatement>() { statement }));
                        break;
                    case "CustomerStatementPayableOrder":
                        e.DataSources.Add(new ReportDataSource("PayableOrders", statement.PayableOrders));
                        break;
                }
            };

            string reportType = "pdf";
            string mimeType;
            string encoding;
            string fileNameExtension;
            string deviceInfo = string.Empty;

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;
            //Render the report            
            renderedBytes = localReport.Render(reportType, deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);

            Response.AddHeader("content-disposition", "attachment; filename=statements-" + DateTime.Now.ToString("yyyy-MMM-dd HH:mm:ss") + "." + fileNameExtension);
            if (model.Format == null)
                return File(renderedBytes, "image/jpeg");
            else if (model.Format.ToUpper() == "PDF")
                return File(renderedBytes, "pdf");
            else
                return File(renderedBytes, "image/jpeg");

        }


    }
}