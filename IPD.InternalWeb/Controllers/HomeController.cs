﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.Helpers;

using System.Web.UI.DataVisualization.Charting;

using Microsoft.Reporting.WebForms;
using IPD.Reports.DataGenerators;

namespace IPD.InternalWeb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Chart()
        {            
            const string chartKey = "IPDDailyActivity-OINV";            
            var chartBytes = HttpContext.Cache[chartKey] as byte[];
            var chart = new System.Web.UI.DataVisualization.Charting.Chart();

            if (chartBytes == null || chartBytes.Length == 0 || System.Diagnostics.Debugger.IsAttached)
            {
                var values = IPD.Reports.DataGenerators.DashboardReports.GetIPDDailyActivity(DateTime.Now, 28);

                chart.BackColor = System.Drawing.Color.Transparent;
                chart.Width = System.Web.UI.WebControls.Unit.Pixel(1100);
                chart.Height = System.Web.UI.WebControls.Unit.Pixel(300);
                chart.Titles.Add(new Title(String.Format("* as of {0}", DateTime.Now.ToString("M/d h:mm:ss tt")), Docking.Top, new System.Drawing.Font("Calibri", 9f, System.Drawing.FontStyle.Regular), System.Drawing.Color.Gray));
                chart.Titles[0].Alignment = System.Drawing.ContentAlignment.TopRight;
                chart.Titles[0].Position.Auto = false;
                chart.Titles[0].Position.Y = 93F; // ((float)chart.Height.Value) - chart.Titles[0].Font.SizeInPoints;
                chart.Titles[0].Position.X = 95F; //((float)chart.Width.Value) - 50;


                ChartArea ca1 = new ChartArea("ca1");
                chart.ChartAreas.Add(ca1);
                ca1.BackColor = System.Drawing.Color.Transparent;

                ca1.AxisY.IsLabelAutoFit = true;
                ca1.AxisY.Title = "# of orders";
                ca1.AxisY.TextOrientation = TextOrientation.Rotated270;
                ca1.AxisY.LabelAutoFitStyle = LabelAutoFitStyles.LabelsAngleStep30;
                ca1.AxisY.LabelStyle.Enabled = true;
                ca1.AxisY.LabelStyle.Font = new System.Drawing.Font("Calibri", 9f, System.Drawing.FontStyle.Regular);
                //ca1.AxisY.Interval = 1;

                ca1.AxisX.IsLabelAutoFit = true;
                ca1.AxisX.LabelStyle.Enabled = true;
                ca1.AxisX.LabelStyle.Font = new System.Drawing.Font("Calibri", 9f, System.Drawing.FontStyle.Regular);

                ca1.AxisX.Interval = 1;
                ca1.AxisX.Enabled = AxisEnabled.True;
                ca1.AxisX.MajorTickMark = new TickMark() { LineDashStyle = ChartDashStyle.Dot, LineColor = System.Drawing.Color.Silver };
                ca1.AxisX.ScaleBreakStyle = new AxisScaleBreakStyle() { LineColor = System.Drawing.Color.Silver };


                var seriesOpen = new System.Web.UI.DataVisualization.Charting.Series("Open");
                seriesOpen.Color = System.Drawing.Color.Goldenrod;

                var seriesOfferred = new System.Web.UI.DataVisualization.Charting.Series("Offerred");
                seriesOfferred.Color = System.Drawing.Color.YellowGreen;

                var seriesInProgress = new System.Web.UI.DataVisualization.Charting.Series("InProgress");
                seriesInProgress.Color = System.Drawing.Color.Green;

                var seriesComplete = new System.Web.UI.DataVisualization.Charting.Series("Complete");
                seriesComplete.Color = System.Drawing.Color.DarkGreen;

                var seriesCancelled = new System.Web.UI.DataVisualization.Charting.Series("Cancelled");
                seriesCancelled.Color = System.Drawing.Color.DarkRed;

                chart.Series.Add(seriesComplete);
                chart.Series.Add(seriesInProgress);
                chart.Series.Add(seriesOfferred);
                chart.Series.Add(seriesOpen);
                chart.Series.Add(seriesCancelled);

                foreach (var series in chart.Series)
                {
                    series.ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.StackedColumn;
                    //series.IsValueShownAsLabel = true;
                    //series.LabelFormat = @"0;0;""";
                    //series.LabelForeColor = System.Drawing.Color.White;
                    //series.Font = new System.Drawing.Font("Calibri", 8f, System.Drawing.FontStyle.Regular);
                }

                chart.Legends.Add(new Legend("Complete")
                {
                    Alignment = System.Drawing.StringAlignment.Center,
                    Docking = Docking.Bottom,
                    IsTextAutoFit = true,
                    LegendStyle = LegendStyle.Row,
                    //Title = "Complete",                     
                    IsDockedInsideChartArea = true,
                    TitleAlignment = System.Drawing.StringAlignment.Center
                });

                for (var day = 0; day < values.Item1.Count; day++)
                {
                    seriesOpen.Points.AddXY(values.Item1[day], values.Item2[day][0]);
                    seriesOfferred.Points.AddXY(values.Item1[day], values.Item2[day][1]);
                    seriesInProgress.Points.AddXY(values.Item1[day], values.Item2[day][2]);
                    seriesComplete.Points.AddXY(values.Item1[day], values.Item2[day][3]);
                    seriesCancelled.Points.AddXY(values.Item1[day], values.Item2[day][4]);
                }

                using (var ms = new System.IO.MemoryStream())
                {
                    chart.SaveImage(ms, System.Web.UI.DataVisualization.Charting.ChartImageFormat.Png);
                    ms.Position = 0;
                    chartBytes = ms.ToArray();
                }                
                HttpContext.Cache.Insert(chartKey, chartBytes, null, DateTime.Now.AddMinutes(10), Cache.NoSlidingExpiration);
            }

            var rtnStream = new System.IO.MemoryStream(chartBytes);            
            return new FileStreamResult(rtnStream, "image/png");

            
        }

        public ActionResult RecentOrders()
        {
            var modelKey = "RecentOrdersViewModel";
            var model = HttpContext.Cache[modelKey] as Models.RecentOrdersViewModel;
            if (model == null || System.Diagnostics.Debugger.IsAttached)
            {
                model = new Models.RecentOrdersViewModel() { RecentOrders = IPD.Reports.DataGenerators.DashboardReports.GetRecentOrders() };
                HttpContext.Cache.Insert(modelKey, model, null, DateTime.Now.AddMinutes(10), Cache.NoSlidingExpiration);
            }

            return View("~/Views/Charts/RecentOrders.cshtml",model);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public ActionResult DriverActivity(string id)
        {
            var model = new Models.DriverActivityViewModel();
            if (!String.IsNullOrEmpty(id))
            {
                model.MobileNumber = id;
                return DriverActivity(model);
            }
            else
                return View("~/Views/Home/DriverActivity.cshtml", model);
        }
        
        [HttpPost]
        public ActionResult DriverActivity(Models.DriverActivityViewModel model)
        {
            var progress = "A";
            try
            {
                var mongoDb = IPD.Data.Mongo.IPDMongo.GetIPDDb(System.Configuration.ConfigurationManager.ConnectionStrings["mongo"].ConnectionString);
                progress += ";B";
                var driverService = new IPD.Service.DriverService();
                if (model.Driver == null || model.MobileNumber != model.Driver.DRV_MOBILE_NUMBER)
                {
                    using (var ipdEntities = new IPD.Data.Oracle.Entities())
                    {
                        progress += String.Format(";B.1 - mobileNumber={0};", model.MobileNumber);
                        model.Driver = driverService.GetDriverInfo(ipdEntities, model.MobileNumber);
                    }
                    model.DriverGPS = null;
                }

                progress += ";C";
                if (model.DriverGPS == null)
                    model.DriverGPS = driverService.GetDriverCurrentGPSStatus(mongoDb, model.MobileNumber);

                progress += ";D";
                if (model.DriverGPS != null)
                    model.GPSDatas = driverService.GetDriverLocationHistory(mongoDb, model.DriverGPS.strDrvGPSDrvId, model.HistoryLimit, (model.HistoryPageNumber - 1) * model.HistoryLimit);

                progress += ";E";
                if (model.DriverGPS != null)
                    model.DriverInMessages = driverService.GetDriverInMessage(mongoDb, model.DriverGPS.strDrvGPSDrvId, model.HistoryLimit, (model.HistoryPageNumber - 1) * model.HistoryLimit);

                progress += ";F";
                return View("~/Views/Home/DriverActivity.cshtml", model);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("progress: {0}; Msg: {1}:", progress, ex.GetBaseException().Message));
            }
        }

        [HttpGet]
        public JsonResult DriverSearch(string term)
        {
            using(var ipdEntities = new Data.Oracle.Entities())
            {
                var driverService = new IPD.Service.DriverService();
                var rtn = driverService.SearchForDriver(ipdEntities, term)
                    .Take(200)
                    .ToList()
                    .Select( d => new {
                        value = d.MobileNumber,
                        label = d.MobileNumber + " " + d.Name
                    });
               return Json(rtn.ToArray(), JsonRequestBehavior.AllowGet);
            }

            return Json(null);
        }

        [HttpGet]
        public ActionResult PaymentProcessing()
        {
            ViewBag.Message = "Order History page.";
            var model = new Models.PaymentProcessingViewModel();
            model.AsOfDate = DateTime.UtcNow;
            return View("~/Views/Home/PaymentProcessing.cshtml", model);
        }

        [HttpPost]
        public ActionResult PaymentProcessing(Models.PaymentProcessingViewModel model)
        {

            var settlementGenerator = new IPD.Reports.DataGenerators.Settlement();
            List<IPD.Reports.DataSetTypes.DriverStatement> statements;
            using (var oracleEntities = new Data.Oracle.Entities())
            {
                List<IPD.Reports.DataSetTypes.PayableOrder> additionalItems = null;
                if (!String.IsNullOrEmpty(model.AdditionalPayableItems))
                {
                    additionalItems = new List<IPD.Reports.DataSetTypes.PayableOrder>();
                    var lines = model.AdditionalPayableItems.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                    foreach (var line in lines)
                    {
                        var vals = line.Split(",".ToCharArray());
                        decimal offerAmount = 0;
                        decimal.TryParse(vals[3], out offerAmount);
                        decimal feeAmount = 0;
                        decimal.TryParse(vals[4], out feeAmount);

                        additionalItems.Add(new IPD.Reports.DataSetTypes.PayableOrder()
                        {
                            DriverNumber = vals[0],
                            OrderNumber = vals[1],
                            ShipperName = vals[2],
                            OfferAmount = offerAmount,
                            FeeAmount = feeAmount,
                            NetPayableAmount = offerAmount - feeAmount
                        });
                    }
                }
                statements = settlementGenerator.GenerateDriverStatements(oracleEntities, model.AsOfDate, additionalItems);


                if (model.Format.ToLower() == "text")
                {
                    model.DriverStatements = statements;

                    if (model.ExecuteUpdates)
                    {
                        var updateSuccess = false;
                        try
                        {
                            var recsUpdated = oracleEntities.Database.ExecuteSqlCommand(model.OracleUpdateStatements);
                            model.OracleUpdateStatementResult = string.Format("{0} records updated", recsUpdated);
                            updateSuccess = true;
                        }
                        catch (Exception e)
                        {
                            model.OracleUpdateStatementResult = e.GetBaseException().Message;
                        }

                        if (updateSuccess)
                        {
                            try
                            {
                                var mongoDb = IPD.Data.Mongo.IPDMongo.GetIPDDb(System.Configuration.ConfigurationManager.ConnectionStrings["mongo"].ConnectionString);
                                var orderNumbers = (from o in model.PayableOrderNumbers select new MongoDB.Bson.BsonString(o)).ToList();
                                var ordersCollection = mongoDb.GetCollection<IPD.Data.Mongo.Order>("Order");
                                var result = ordersCollection.Update(MongoDB.Driver.Builders.Query.In("strOrdNumber", orderNumbers), 
                                    MongoDB.Driver.Builders.Update<IPD.Data.Mongo.Order>.Set(o => o.Status, "PAYMENT_COMPLETE"),
                                    MongoDB.Driver.UpdateFlags.Multi);

                                model.MongoUpdateResults = string.Format("Ok: {0}; {1} Documents Affected; Msg: {2}", result.Ok, result.DocumentsAffected, result.ErrorMessage);
                            }
                            catch (Exception e)
                            {
                                model.MongoUpdateResults = e.GetBaseException().Message;
                                updateSuccess = false;  
                            }
                        }

                        model.ExecuteUpdates = false;
                    }

                    return View("~/Views/Home/PaymentProcessing.cshtml", model);
                }
            }
            model.HasReport = true;
            LocalReport localReport = new LocalReport();
            var dllPath = System.IO.Path.Combine(Server.MapPath(@"/bin"),"IPD.Reports.dll");
            var assembly = System.Reflection.Assembly.LoadFrom(dllPath);
            //localReport.LoadReportDefinition(assembly.GetManifestResourceStream("IPD.Reports.Reports.DriverStatement.rdlc"));
            localReport.LoadReportDefinition(assembly.GetManifestResourceStream("IPD.Reports.Reports.DriverStatementMany.rdlc"));
            localReport.LoadSubreportDefinition("DriverStatement", assembly.GetManifestResourceStream("IPD.Reports.Reports.DriverStatement.rdlc"));
            localReport.LoadSubreportDefinition("StatementPayableOrder", assembly.GetManifestResourceStream("IPD.Reports.Reports.StatementPayableOrder.rdlc"));

            if (model.MessageHeader != null || model.Message != null)
            {
                var parms = new ReportParameterCollection();
                parms.Add(new ReportParameter("Message", model.Message));
                parms.Add(new ReportParameter("MessageHeader", model.MessageHeader));
                localReport.SetParameters(parms);
            }
            localReport.DataSources.Add(new ReportDataSource("DriverStatements", statements));


            localReport.SubreportProcessing += (o, e) => {
                var statement = statements.Where(s => s.DriverId == new Guid(e.Parameters["DriverId"].Values[0].ToString())).FirstOrDefault();
                switch (e.ReportPath)
                { 
                    case "DriverStatement":                        
                        e.DataSources.Add(new ReportDataSource("DriverStatement", new List<IPD.Reports.DataSetTypes.DriverStatement>() {statement}));
                        break;
                    case "StatementPayableOrder":
                        e.DataSources.Add(new ReportDataSource("PayableOrders", statement.PayableOrders));
                    break;
                }
            };

            string reportType = "pdf";
            string mimeType;
            string encoding;
            string fileNameExtension;
            string deviceInfo = string.Empty ;

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;
            //Render the report            
            renderedBytes = localReport.Render(reportType, deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);
            
            Response.AddHeader("content-disposition", "attachment; filename=statements-" + DateTime.Now.ToString("yyyy-MMM-dd HH:mm:ss") + "." + fileNameExtension);
            if (model.Format == null)
                return File(renderedBytes, "image/jpeg");
            else if (model.Format.ToUpper() == "PDF")
                return File(renderedBytes, "pdf");
            else
                return File(renderedBytes, "image/jpeg");

        }
    
        [HttpGet]
        public ActionResult CourierPwdReset()
        {
            var model = new Models.CourierPwdResetViewModel();
            return View("~/Views/Home/CourierPwdReset.cshtml", model);
        }

        [HttpPost]
        public JsonResult CourierPwdReset(Models.CourierPwdResetViewModel model)
        {
            if (model.SecurityToken.ToLower() != "superdelivery")
            {
                model.Message = "Security Token is invalid";
                return Json(model);
            }

            using (var ipdEntities = new IPD.Data.Oracle.Entities())
            {
                foreach (var item in model.ResetItems)
                {
                    //var salt = Guid.NewGuid().ToOracleString();
                    item.Result = string.Empty;
                    if (!String.IsNullOrEmpty(item.UserName) && !String.IsNullOrEmpty(item.Password))
                    {
                        if (item.Password.Trim().Length < 8)
                            item.Result = "Password inadequate";
                        else
                        {
                            var customerUsr = ipdEntities.CUSTOMER_USR.Where(x => x.LoginName == item.UserName).FirstOrDefault();
                            if (customerUsr == null)
                                item.Result = "UserNotFound";
                            else
                            {
                                try
                                {
                                    var salt = String.IsNullOrEmpty(customerUsr.PasswordSalt) ? "THIIhtTEJjyWayMt" : customerUsr.PasswordSalt;
                                    var newPwd = HashPassword(item.Password.Trim(), salt);

                                    customerUsr.Password = newPwd;
                                    customerUsr.PasswordSalt = salt;

                                    ipdEntities.SaveChanges();
                                    item.Result = "success";
                                }
                                catch (Exception e)
                                {
                                    item.Result = String.Format("Exception: {0}", e.GetBaseException().Message);
                                }
                            }
                        }
                    }
                }
                model.Message = "Results as indicated";
            }
            return Json(model);
        }

        [HttpGet]
        public ActionResult OAccess()
        {
            return View("~/Views/Home/OAccess.cshtml", new Models.OAccess());
        }

        [HttpPost]
        public JsonResult OAccess(Models.OAccess model)
        {
            if (model.SecurityToken != "comeandgetit")
            {
                model.Message = "Error with token";
                model.IsError = true;
                return Json(model);
            }
            var result = new System.Collections.ArrayList();
            using (var ipdEntities = new IPD.Data.Oracle.Entities())
            using (var connection = new Oracle.ManagedDataAccess.Client.OracleConnection())
            {
                if (!String.IsNullOrEmpty(model.Statement))
                {
                    try
                    {
                        model.Message = String.Empty;
                        model.IsError = false;
                        //ug, some wholly inadequate sql abuse prevention
                        var statement = model.Statement.Trim().TrimEnd(";".ToCharArray());
                        if (!statement.StartsWith("select", StringComparison.OrdinalIgnoreCase) || statement.Contains(";"))
                        {
                            model.Message ="Bad Bad Bad";
                            model.IsError = true;
                            return Json(model);
                        }
                        var badWords = new string[] { "update", "delete", "insert", "drop", "alter", "grant", "revoke", "purge", "rename", "truncate", "lock", "merge" };
                        foreach (var badWord in badWords)
                        {
                            if (statement.IndexOf(badWord, StringComparison.OrdinalIgnoreCase) > -1)
                            {
                                model.Message = "Bad Bad Bad";
                                model.IsError = true;
                                return Json(model);
                            }
                        }

                        connection.ConnectionString = ipdEntities.Database.Connection.ConnectionString;
                        connection.Open();

                        var command = connection.CreateCommand();
                        //statement = "select * from driver where drv_id = '" + new Guid("{13132D8BD80E00EEE053C0A8647500EE}").ToByteArray() + "'";
                        command.CommandText = statement;

                        var reader = command.ExecuteReader();
                        result.Add(Enumerable.Range(0, reader.FieldCount)
                            .Select(reader.GetName)
                            .ToArray());
                        while (reader.Read())
                        {
                            object[] values = new object[reader.FieldCount];
                            reader.GetValues(values);
                            for (var i = 0; i < values.Length; i++)
                            {
                                var o = values[i];
                                if (o is byte[] && ((byte[])o).Length == 16)
                                    values[i] = new Guid((byte[])o).ToOracleGuid().ToOracleString();
                                else if (o is DateTime)
                                    values[i] = ((DateTime) o).ToString("yyyy-MM-ddThh:mm:ss");
                            }
                            result.Add(values);
                        }

                        model.result = result.ToArray();
                        model.Message = string.Format("{0} Rows", model.result.Length);
                        model.IsError = false;
                    }
                    catch (Exception e)
                    {
                        model.Message = e.GetBaseException().Message;
                        model.IsError = true;
                    }
                }
            }
            return Json(model);
        }


        [HttpGet]
        public ActionResult DriverList()
        {
            return View("~/Views/Home/DriverList.cshtml");
        }

        [HttpPost]
        public JsonResult DriverList(int pSkip, int pTake)
        //public JsonResult DriverList(string pSkip, string pTake)
        {
            //var skip = int.Parse(pSkip);
            //var take = int.Parse(pTake);
            var skip = pSkip;
            var take = pTake;
            take = take < 1 ? 100 : take;
            skip = skip * take;
            using(var ipdEntities = new Data.Oracle.Entities())
            {
                var rtn = (from d in ipdEntities.DRIVERs
                           select new
                           {
                               d.DRV_NAME,
                               d.DRV_MOBILE_NUMBER,
                               d.DRV_EMAIL,
                               d.DRV_BUSINESS_NAME,
                               d.DRV_STREET,
                               d.DRV_CITY,
                               d.DRV_STATE,
                               d.DRV_ZIP,
                               d.DRV_STATUS,
                               d.DRV_ACTIVE_STATUS,
                               d.DRV_VEHICLE_TYPE,
                               d.DRV_VEHICLE_ACCESSORIALS,
                               d.DRV_VEHICLE_PACKAGE_LENGTH,
                               d.DRV_VEHICLE_PACKAGE_WEIGHT,
                               d.DRV_INTRODUCE,
                               d.DRV_LAST_UPD_TIME
                           })
                           .OrderBy(d => d.DRV_NAME)
                           .Skip(skip)
                           .Take(take)
                           .ToList()
                           .Select(d => new object [] 
                           {
                               d.DRV_NAME.Replace(",", " "),
                               d.DRV_LAST_UPD_TIME.ToString("M/d/yy h:mm tt"),
                               d.DRV_MOBILE_NUMBER,
                               d.DRV_EMAIL,
                               d.DRV_BUSINESS_NAME,
                               String.Format("{0} {1}, {2} {3}", d.DRV_STREET, d.DRV_CITY, d.DRV_STATE, d.DRV_ZIP),
                               d.DRV_STATUS,
                               d.DRV_ACTIVE_STATUS,
                               d.DRV_VEHICLE_TYPE,
                               d.DRV_VEHICLE_ACCESSORIALS,
                               String.Format(@"{0}lbs, {1}""", d.DRV_VEHICLE_PACKAGE_WEIGHT, d.DRV_VEHICLE_PACKAGE_LENGTH),
                               d.DRV_INTRODUCE == null ? "" 
                                    : d.DRV_INTRODUCE.Length < 40 ? d.DRV_INTRODUCE
                                    : d.DRV_INTRODUCE.Substring(0,37) + "..." 
                           })
                           //.OrderBy(d => d.
                           .ToList()
                           ;
                return Json(rtn);
            }
        }

        private string HashPassword(string password, string salt)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(password + salt);
            var hasher = System.Security.Cryptography.SHA256Cng.Create();
            byte[] hash = hasher.ComputeHash(bytes);
            var hashedPwd = new System.Text.StringBuilder();
            foreach (byte x in hash)
            {
                hashedPwd.AppendFormat("{0:x2}", x);
            }
            return hashedPwd.ToString();
        }



    }
}