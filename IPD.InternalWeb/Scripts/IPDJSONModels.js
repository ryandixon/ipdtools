﻿function AccountingBatch() {
    var self = this;
    self.id = $('#SelectedBatchId').val();
    self.Type = null;
    self.CreatedDateTime = Date();
    self.Status = null;
    self.Description = $('#SelectedBatch_Description').val();
    self.UpdatedBy = $('#SelectedBatch_UpdatedBy').val();
    self.OrderAccountingBatches = null;
    //self.ListItem = $('#batchList').find("[data-batchid='" + $('#SelectedBatchId').val() + "']");
    //This needs to be a method call instead of a property to avoid a circular reference, which prevents JSON.Stringify from working on this object
    self.GetListItem = function () {
        return $('#batchList').find("[data-batchid='" + self.id + "']");
    }

}

function OrderAccountingBatch(jsonResponse) {
    var self = this;
    self.id = jsonResponse.id;
    self.orderId = jsonResponse.OrderId;
    self.orderNumber = jsonResponse.OrderNumber;
    self.status = jsonResponse.Status;
    self.CustomerName = jsonResponse.CustomerName;
    self.driverName = jsonResponse.DriverName;
    self.TotalDue = jsonResponse.TotalDue;
    self.CompletedDate = jsonResponse.CompletedDate;
    self.DriverMobileNumber = jsonResponse.DriverMobileNumber;
    self.IsApprovedForPmt = jsonResponse.IsApprovedForPmt;

    self.THead = "<thead><th>Order Number</th><th>Pmt Status</th><th>Date Completed</th><th>Customer</th><th>Driver</th><th>Amount</th></thead><tbody>";
    self.toTR = function () {
        var rtn = "<tr class='batchOrderRow' data-orderid'" + self.orderId + "'>";
        rtn += "<td>" + self.orderNumber + "</td>";
        rtn += "<td>" + self.status + "</td>";
        rtn += "<td>" + self.CompletedDate + "</td>";
        rtn += "<td>" + self.CustomerName + "</td>";
        rtn += "<td><a " + (self.IsApprovedForPmt == false ? "class='notApprovedForPmt'" : "") +  "href='/driver/profile/" + self.DriverMobileNumber + "' target='_blank'>" + self.driverName + "</a></td>";
        rtn += "<td>" + self.TotalDue + "</td>";
        rtn += "</tr>";
        return rtn;
    }
}

function GetStatementsRequest() {
    var self = this;
    self.BatchId = $('#SelectedBatchId').val();
    self.MessageHeader = $('#StatementsRequestModel_MessageHeader').val();
    self.Message = $('#StatementsRequestModel_Message').val();
    self.AdditionalPayableItems = $('#StatementsRequestModel_AdditionalPayableItems').val();
    self.Format = $('#StatementsRequestModel_SatementFormat').val();
}