﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IPD.InternalWeb.Startup))]
namespace IPD.InternalWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
