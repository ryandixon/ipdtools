﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Reports.DataSetTypes
{
    public class DriverStatement
    {
        public DriverStatement() { PayableOrders = new List<PayableOrder>(); }

        public static List<DriverStatement> GenerateDriverStatements(Func<List<DriverStatement>> generateFunction)
        {
            return generateFunction();
        }
        public Guid Id { get; set; }
        public Guid DriverId { get; set; }
        public string DriverNumber { get; set; }
        public string OrderNumber { get; set; }
        public DateTime StatementDate { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "First Name")]
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string StreetAddress1 { get; set; }
        public string Suite { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string FullAddress 
        {
            get
            {
                return string.Format("{0} {1} {2} {3} {4}",
                    StreetAddress1,
                    String.IsNullOrEmpty(Suite) ? "" : ", " + Suite,
                    String.IsNullOrEmpty(City) ? "" : " " + City,
                    String.IsNullOrEmpty(State) ? "" : ", " + State,
                    String.IsNullOrEmpty(Zip) ? "" : " " + Zip);
            }
        }

        public string EmailAddress { get; set; }
        public string DriverMobileNumber { get; set; }

        public decimal TotalAmountEarned { get; set; }
        public decimal TotalIPDFees { get; set; }
        public decimal NetPayableAmount { get; set; }

        public List<PayableOrder> PayableOrders { get; set; }

        public string ToCSV()
        {
            return string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}", 
                this.FirstName,
                this.LastName,
                this.EmailAddress, 
                this.PhoneNumber, 
                StatementDate, 
                TotalAmountEarned,
                TotalIPDFees, 
                NetPayableAmount, 
                (StreetAddress1 ?? "").Replace(",", ""), 
                (Suite ?? "").Replace(",", ""), 
                (City ?? "").Replace(",", ""), 
                State, 
                Zip);

        }

    }
}
