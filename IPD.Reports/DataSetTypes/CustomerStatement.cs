﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Reports.DataSetTypes
{
    public class CustomerStatement
    {
        public CustomerStatement() { PayableOrders = new List<PayableOrder>(); }

        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public string InvoiceNumber { get; set; }
        public string CustomerNumber { get; set; }
        public DateTime StatementDate { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Business Name")]
        public string BusinessName { get; set; }
        public string ContactName { get; set; }
        public string ContactPhoneNumber { get; set; }
        public string StreetAddress1 { get; set; }
        public string Suite { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string FullAddress
        {
            get
            {
                return string.Format("{0} {1} {2} {3} {4}",
                    StreetAddress1,
                    String.IsNullOrEmpty(Suite) ? "" : ", " + Suite,
                    String.IsNullOrEmpty(City) ? "" : " " + City,
                    String.IsNullOrEmpty(State) ? "" : ", " + State,
                    String.IsNullOrEmpty(Zip) ? "" : " " + Zip);
            }
        }

        public string EmailAddress { get; set; }

        public decimal TotalAmountDue { get; set; }
        public decimal TotalIPDFees { get; set; }
        public decimal NetAmountDue { get; set; }


        public List<PayableOrder> PayableOrders { get; set; }

        public static string GenerateInvoiceNumber(DateTime statementDate, Guid customerId)
        {
            if (statementDate == DateTime.MinValue)
                throw new Exception("Invoice Statement Date is not valid");

            var customerIdString = customerId.ToOracleString();
            int num = Int32.Parse(customerIdString.Substring(customerIdString.Length - 5), System.Globalization.NumberStyles.HexNumber);
            var rnd = new Random(num);
            //Really need to have a unique customer code here that is much shorter than a GUID and permenant to the customer!
            
            var customerCode = string.Format("{0}{1}", customerIdString.Substring(customerIdString.Length - 5), NewId(rnd, 3));

            return String.Format("{0}{1}-{2}", statementDate.Year.ToString().Substring(2), statementDate.DayOfYear, customerCode);
        }

        private static string NewId(Random rnd, int length)
        {
            const string stringvalues = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            string rtn = string.Empty;

            while (rtn.Length < length)
            {
                rtn += stringvalues.Substring(rnd.Next(0, stringvalues.Length - 1), 1);
            }

            return rtn;
        }

        public string ToCSV()
        {
            return string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}",
                this.BusinessName,
                this.EmailAddress,
                this.ContactPhoneNumber,
                this.InvoiceNumber,
                StatementDate,
                TotalAmountDue,
                TotalIPDFees,
                NetAmountDue,
                (StreetAddress1 ?? "").Replace(",", ""),
                (Suite ?? "").Replace(",", ""),
                (City ?? "").Replace(",", ""),
                State,
                Zip);

        }
    }
}
