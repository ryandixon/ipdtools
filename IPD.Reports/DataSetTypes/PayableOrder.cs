﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Reports.DataSetTypes
{
    public class PayableOrder
    {
        public Guid OrderId { get; set; }
        public Guid DriverId { get; set; }

        public Guid CustomerId { get; set; }

        private string driverNumber;
        [DataType(DataType.Text)]
        [Display(Name = "Driver Number")]
        public string DriverNumber
        {
            get { return driverNumber; }
            set
            {
                driverNumber = value.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").Replace(".", "").Replace("+","");
            }
        }

        [DataType(DataType.Text)]
        [Display(Name = "Driver Name")]
        public string DriverName { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Order Number")]
        public string OrderNumber { get; set; }
        public string ReferenceNumber { get; set; }
        [DataType(DataType.Text)]
        [Display(Name = "Shipper Name")]
        public string ShipperName { get; set; }
        public DateTime? OrderCompletionDate { get; set; }
        [DataType(DataType.Text)]
        [Display(Name = "1st Stop Address")]
        public string FirstStopAddress { get; set; }
        [DataType(DataType.Text)]
        [Display(Name = "Last stop Address")]
        public string LastStopAddress { get; set; }

        [DataType(DataType.Currency)]
        [Display(Name = "Offer Amount")]
        public decimal OfferAmount { get; set; }
        [DataType(DataType.Currency)]
        [Display(Name = "Fee Amount")]
        public decimal FeeAmount { get; set; }
        [DataType(DataType.Currency)]
        [Display(Name = "Payable Amount")]
        public decimal NetPayableAmount { get; set; }
    }
}
