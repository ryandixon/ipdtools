﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Reports.DataSetTypes
{
    public class RecentOrder
    {
        public string OrderNumber { get; set; }
        public string Organization { get; set; }
        public DateTime OrderCreateDateTime { get; set; }
        public string OrderStatus { get; set; }
        public string DriverName { get; set; }
        public decimal OfferAmount { get; set; }
        public string Pickup { get; set; }
        public string LastStop { get; set; }
        public string MostRecentEvent { get; set; }
    }

}
