﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IPD.Reports.DataSetTypes;
using System.Data.Entity.SqlServer;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using IPD.Data.Mongo;

namespace IPD.Reports.DataGenerators
{
    public class Settlement
    {

        public List<DriverStatement> GenerateDriverStatements(IPD.Data.Accounting.IPDAccounting ipdAccounting, IPD.Data.Accounting.AccountingBatch accountingBatch, List<PayableOrder> additionalPOs = null)
        {
            var rtn = new List<DriverStatement>();
            var statuses = new string[] { "DELIVERYCOMPLETED" };
            //var completedOrders = new List<Tuple<Guid, Guid, DateTime>>();

            if (accountingBatch.Type != "Payment")
                throw new Exception("Accounting Batch is not a payment batch.");

            /*
             * NOTE: Two big challenges are shaping the structure of this code that I hope get fixed soon: 
             *  1. The ORD_ID in oracle only has 12 bytes in the data instead of 16. This prevents using the entity because a GUID cannot be instantiated with a 12 byte value
             *  2. The datetime that an order status changes is currently only in the mongoDB collection; I would like to have this in Oracle.
             */

            //

            var completeEvents = (from oab in ipdAccounting.OrderAccountingBatches
                                  where oab.BatchId == accountingBatch.id
                                  select new
                                  {
                                      driverId = oab.Order.DriverId,
                                      oab.OrderId,
                                      oab.Order.OrderNumber,
                                      CompletionTime = oab.Order.ORD_COMPLETE_TIME
                                  })
                                 .ToList();


            var driverIds = completeEvents.Select(x => x.driverId).Distinct().ToList();

            var driverNumbers = new List<string>();
            if (additionalPOs != null)
            {
                DateTimeOffset utcCutoffDateTime = accountingBatch.CutoffDateTime.Value.ToUniversalTime();
                var bsonDate = BsonDateTime.Create(utcCutoffDateTime);
                DateTime cutoffDate = utcCutoffDateTime.DateTime;

                additionalPOs.ForEach(po => po.OrderCompletionDate = utcCutoffDateTime.LocalDateTime);
                driverNumbers = (from t in additionalPOs
                                 select t.DriverNumber)
                                    .Distinct()
                                    .ToList();
                foreach (var driverNumber in driverNumbers)
                {
                    var driverId = ipdAccounting.Drivers.Where(d => d.DriverNumber == driverNumber).Select(d => d.id).FirstOrDefault();
                    if (driverId != null && !driverIds.Contains(driverId))
                        driverIds.Add(driverId);
                }
            }

            //loop through all the completed orders by each driver building h

            //TODO Parallel process this loop!
            foreach (var driver in from d in ipdAccounting.Drivers
                                   where driverIds.Contains(d.id)
                                   select d)
            {
                MongoDatabase mongoDb = null;
                var statement = new DriverStatement
                {
                    DriverId = driver.id,
                    FirstName = driver.Name.Replace(",", " "),
                    StatementDate = DateTime.Now,
                    StreetAddress1 = driver.DRV_STREET,
                    Suite = driver.DRV_SUITE,
                    City = driver.DRV_CITY,
                    State = driver.DRV_STATE,
                    Zip = driver.DRV_ZIP,
                    PhoneNumber = driver.DRV_CONTACT_PHONE,
                    DriverMobileNumber = driver.MobileNumber,
                    DriverNumber = driver.DriverNumber,
                    EmailAddress = driver.DRV_EMAIL
                };

                var driverCompletedOrders = (from ce in completeEvents
                                             where ce.driverId == statement.DriverId
                                             select ce.OrderNumber)
                                                .Distinct()
                                                .ToList();
                #region loop through completed orders and gather addtional info - customer, stops, etc.
                foreach (var item in from o in ipdAccounting.Orders
                                     join c in ipdAccounting.Customers on o.CustomerId equals c.id
                                     where statuses.Contains(o.Status)
                                      && driverCompletedOrders.Contains(o.OrderNumber)
                                     select new
                                     {
                                         OrderId = o.id,
                                         o.OrderNumber,
                                         c.BusinessName,
                                         CustomerId = c.id,
                                         o.CreateTime,
                                         o.ORD_COMPLETE_TIME,
                                         o.ORD_CUSTOMER_REFERENCE,
                                         o.Status,
                                         o.TotalDue,
                                         TotalRate = o.ORD_PAY_PRICES // adj.TotalRate,
                                     })
                {

                    var payableOrder = new PayableOrder
                    {
                        OrderId = item.OrderId,
                        DriverId = statement.DriverId,
                        CustomerId = item.CustomerId,
                        OrderNumber = item.OrderNumber,
                        ShipperName = item.BusinessName,
                        ReferenceNumber = item.ORD_CUSTOMER_REFERENCE,
                        OfferAmount = item.TotalRate ?? 0M,
                        OrderCompletionDate = item.ORD_COMPLETE_TIME
                    };

                    //fetch completion datetime from MongoDB. This should populate in Oracle in a future release from wintao so, code for that now
                    // when it's populating in oracle, this code can be removed
                    // -RDD 5/27/2015
                    if (!payableOrder.OrderCompletionDate.HasValue)
                    {
                        if (mongoDb == null)
                        {
                            var mongoCxnString = System.Configuration.ConfigurationManager.ConnectionStrings["mongo"].ConnectionString;
                            var mongoClient = new MongoClient(mongoCxnString);
                            var mongoUrl = new MongoUrl(mongoCxnString);
                            mongoDb = mongoClient.GetServer().GetDatabase(mongoUrl.DatabaseName);
                        }

                        payableOrder.OrderCompletionDate = GetOrderCompletionDate(mongoDb, payableOrder.OrderNumber);
                    }

                    ApplyStopInfo(ipdAccounting, payableOrder);

                    //Round the fee up to the nearest penny
                    payableOrder.FeeAmount = Math.Ceiling(((item.TotalRate ?? 0M) * .03M) * 100) / 100M;
                    if (payableOrder.FeeAmount < 1.5M) payableOrder.FeeAmount = 1.5M;
                    payableOrder.NetPayableAmount = payableOrder.OfferAmount - payableOrder.FeeAmount;

                    statement.PayableOrders.Add(payableOrder);
                }
                #endregion

                if (driverNumbers.Contains(statement.DriverNumber))
                    statement.PayableOrders.AddRange(additionalPOs.Where(po => po.DriverNumber == statement.DriverNumber));

                if (statement.PayableOrders.Count > 0)
                {
                    statement.TotalAmountEarned = statement.PayableOrders.Sum(po => po.OfferAmount);
                    statement.TotalIPDFees = statement.PayableOrders.Sum(po => po.FeeAmount);
                    statement.NetPayableAmount = statement.PayableOrders.Sum(po => po.NetPayableAmount);

                    rtn.Add(statement);
                }
            }

            return rtn;
        }

        public List<CustomerStatement> GenerateCustomerStatements(IPD.Data.Accounting.IPDAccounting ipdAccounting, IPD.Data.Accounting.AccountingBatch accountingBatch)
        {
            var rtn = new List<CustomerStatement>();

            var statuses = new string[] { "DELIVERYCOMPLETED", "PAYMENT_COMPLETE" };
            //var completedOrders = new List<Tuple<Guid, Guid, DateTime>>();

            if (accountingBatch.Type != "Collection")
                throw new Exception("Accounting Batch is not a payment batch.");

            //Get info on each order included in this batch that should be billed for
            var completeEvents = (from oab in ipdAccounting.OrderAccountingBatches
                                  where oab.BatchId == accountingBatch.id
                                  select new
                                  {
                                      customerId = oab.Order.CustomerId,
                                      oab.OrderId,
                                      oab.Order.OrderNumber,
                                      CompletionTime = oab.Order.ORD_COMPLETE_TIME
                                  })
                                 .ToList();
            var customerIds = completeEvents.Select(x => x.customerId).Distinct().ToList();

            //TODO Parallel process this loop!
            foreach (var customer in from c in ipdAccounting.Customers
                                   where customerIds.Contains(c.id)
                                   select c)
            {
                MongoDatabase mongoDb = null;
                var statement = new CustomerStatement
                {
                    CustomerId = customer.id,
                    BusinessName = customer.BusinessName,
                    StatementDate = DateTime.Now,                    
                    StreetAddress1 = !String.IsNullOrEmpty(customer.BillingAddressStreet) ? customer.BillingAddressStreet : customer.PhysicalAddressStreet,
                    Suite = !String.IsNullOrEmpty(customer.BillingAddressStreet) ? customer.BillingAddressSuite : customer.PhysicalAddressSuite,
                    City = !String.IsNullOrEmpty(customer.BillingAddressStreet) ? customer.BillingAddressCity : customer.PhysicalAddressCity,
                    State = !String.IsNullOrEmpty(customer.BillingAddressStreet) ? customer.BillingAddressState : customer.PhysicalAddressState,
                    Zip = !String.IsNullOrEmpty(customer.BillingAddressStreet) ? customer.BillingAddressZip : customer.PhysicalAddressZip,
                    ContactPhoneNumber = customer.PrimaryContactPhone,
                    ContactName = customer.PrimaryContactName,                    
                    EmailAddress = customer.PrimaryContactEmail
                };

                statement.InvoiceNumber = CustomerStatement.GenerateInvoiceNumber(statement.StatementDate, statement.CustomerId);

                var customerOrders = (from e in completeEvents
                                     where e.customerId == statement.CustomerId
                                     select e.OrderNumber)
                                    .Distinct()
                                    .ToList();

                //For the customer, loop through all the orders and build info for invoice creation
                foreach (var item in from o in ipdAccounting.Orders
                                     join d in ipdAccounting.Drivers on o.DriverId equals d.id
                                     where statuses.Contains(o.Status)
                                      && customerOrders.Contains(o.OrderNumber)
                                     select new
                                     {
                                         OrderId = o.id,
                                         DriverId = d.id,
                                         o.OrderNumber,
                                         DriverName = d.Name,
                                         d.MobileNumber,
                                         o.CreateTime,
                                         o.ORD_COMPLETE_TIME,
                                         o.ORD_CUSTOMER_REFERENCE,
                                         o.Status,
                                         o.TotalDue,
                                         TotalRate = o.ORD_PAY_PRICES // adj.TotalRate,
                                     })
                {

                    var payableOrder = new PayableOrder
                    {
                        OrderId = item.OrderId,
                        DriverId = item.DriverId,
                        CustomerId = statement.CustomerId,
                        OrderNumber = item.OrderNumber,
                        DriverName = item.DriverName.Replace(","," "),
                        DriverNumber = item.MobileNumber,
                        ReferenceNumber = item.ORD_CUSTOMER_REFERENCE,
                        OfferAmount = item.TotalRate ?? 0M,
                        OrderCompletionDate = item.ORD_COMPLETE_TIME
                    };

                    //fetch completion datetime from MongoDB. This should populate in Oracle in a future release from wintao so, code for that now
                    // when it's populating in oracle, this code can be removed
                    // -RDD 5/27/2015
                    if (!payableOrder.OrderCompletionDate.HasValue)
                    {
                        if (mongoDb == null)
                        {
                            var mongoCxnString = System.Configuration.ConfigurationManager.ConnectionStrings["mongo"].ConnectionString;
                            var mongoClient = new MongoClient(mongoCxnString);
                            var mongoUrl = new MongoUrl(mongoCxnString);
                            mongoDb = mongoClient.GetServer().GetDatabase(mongoUrl.DatabaseName);
                        }
                        payableOrder.OrderCompletionDate = GetOrderCompletionDate(mongoDb, payableOrder.OrderNumber);
                    }

                    ApplyStopInfo(ipdAccounting, payableOrder);

                    //Round the fee up to the nearest penny
                    payableOrder.FeeAmount = Math.Ceiling(((item.TotalRate ?? 0M) * .03M) * 100) / 100M;
                    if (payableOrder.FeeAmount < 1.5M) payableOrder.FeeAmount = 1.5M;
                    payableOrder.NetPayableAmount = payableOrder.OfferAmount + payableOrder.FeeAmount;

                    statement.PayableOrders.Add(payableOrder);

                }

                if (statement.PayableOrders.Count > 0)
                {
                    statement.TotalAmountDue = statement.PayableOrders.Sum(po => po.OfferAmount);
                    statement.TotalIPDFees = statement.PayableOrders.Sum(po => po.FeeAmount);
                    statement.NetAmountDue = statement.PayableOrders.Sum(po => po.NetPayableAmount);

                    rtn.Add(statement);
                }

            }

            return rtn;
        }

        private DateTime? GetOrderCompletionDate(MongoDatabase mongoDb, string orderNumber)
        {
            return (from o in mongoDb.GetCollection<Order>("Order").AsQueryable<Order>()
                                            where o.OrderNumber == orderNumber
                                            select o.CompletionTime)
                                                .FirstOrDefault();
        }

        private void ApplyStopInfo(IPD.Data.Accounting.IPDAccounting ipdAccounting, PayableOrder payableOrder)
        {
            var stops = (from o in ipdAccounting.Orders
                         join s in ipdAccounting.Stops on o.id equals s.OrderId
                         where o.OrderNumber == payableOrder.OrderNumber
                         select new
                         {
                             s.SequenceNumber,
                             s.Street,
                             s.City,
                             s.State,
                             s.Zip
                         })
                             .OrderBy(s => s.SequenceNumber)
                             .ToList();

            var firstStop = stops.FirstOrDefault();
            if (firstStop != null)
                payableOrder.FirstStopAddress = string.Format("{0} {1}", firstStop.Street, firstStop.Zip);
            var lastStop = stops.LastOrDefault();
            if (lastStop != null)
                payableOrder.LastStopAddress = string.Format("{0} {1}", lastStop.Street, lastStop.Zip);
        }

        /// <summary>
        /// [DEPRECATED]
        /// </summary>
        /// <param name="ipdEntities"></param>
        /// <param name="asOfDate"></param>
        /// <param name="additionalItems">A list of Tuples with items 1, 2, & 3: [driver number], [Offer Amount], [Fee Amount] </param>
        /// <returns></returns>
        public List<DriverStatement> GenerateDriverStatements(IPD.Data.Oracle.Entities ipdEntities, DateTime asOfDate
            , List<PayableOrder> additionalPOs = null
            )
        {
            var rtn = new List<DriverStatement>();
            var statuses = new string[] { "DELIVERYCOMPLETED" };
            var completedOrders = new List<Tuple<Guid, Guid, DateTime>>();

            var mongoCxnString = System.Configuration.ConfigurationManager.ConnectionStrings["mongo"].ConnectionString;

            var mongoClient = new MongoClient(mongoCxnString);
            var mongoUrl = new MongoUrl(mongoCxnString);
            var mongoDb = mongoClient.GetServer().GetDatabase(mongoUrl.DatabaseName);

            DateTimeOffset utcCutoffDateTime;
            {
                var cutoffDateTime = asOfDate;
                while ((int)cutoffDateTime.DayOfWeek > 0)
                    cutoffDateTime = cutoffDateTime.AddDays(-1);
                //var cutoffDateTime = asOfDate.AddDays(-(((int)asOfDate.DayOfWeek) + 1));
                utcCutoffDateTime = new DateTimeOffset(cutoffDateTime.Year, cutoffDateTime.Month, cutoffDateTime.Day, 0, 0, 0, new TimeSpan(0, 0, 0));
            }

            
            /*
             * NOTE: Two big challenges are shaping the structure of this code that I hope get fixed soon: 
             *  1. The ORD_ID in oracle only has 12 bytes in the data instead of 16. This prevents using the entity because a GUID cannot be instantiated with a 12 byte value
             *  2. The datetime that an order status changes is currently only in the mongoDB collection; I would like to have this in Oracle.
             */

            //Since, as of now, only mongo contains the datetime that the order was completed. Filter completed orders by their completion date in code           
            var bsonDate = BsonDateTime.Create(utcCutoffDateTime);
            var completeEvents = (from o in mongoDb.GetCollection<Order>("Order").AsQueryable<Order>() 
                                      where o.CompletionTime <=bsonDate
                                      && o.Status == "DELIVERYCOMPLETED"
                                  select new
                                  {
                                      driverId = new Guid(HexStringToByteArray(o.strOrdDriverId)),
                                      driverIdString = o.strOrdDriverId,
                                      OrderId = o.strOrdId,
                                      OrderNumber = o.OrderNumber,
                                      CompletionTime = o.CompletionTime
                                  })
                                  .ToList();
            var driverIds = completeEvents.Select(x => x.driverId).Distinct().ToList();

            var driverNumbers = new List<string>();
            if (additionalPOs != null)
            {
                additionalPOs.ForEach(po => po.OrderCompletionDate = utcCutoffDateTime.LocalDateTime);
                driverNumbers = (from t in additionalPOs
                                     select t.DriverNumber)
                                    .Distinct()
                                    .ToList();
                foreach (var driverNumber in driverNumbers)
                {
                    var driverId = ipdEntities.DRIVERs.Where(d => d.DRV_NUMBER == driverNumber).Select(d => d.DRV_ID).FirstOrDefault();
                    if (driverId != null && !driverIds.Contains(driverId))
                        driverIds.Add(driverId);
                }
            }

            foreach (var driver in from d in ipdEntities.DRIVERs
                                   where driverIds.Contains(d.DRV_ID)
                                   select d)
            {
                var statement = new DriverStatement
                {
                    DriverId = driver.DRV_ID,
                    FirstName = driver.DRV_NAME.Replace(",", " "), 
                    StatementDate = DateTime.Now,
                    StreetAddress1 = driver.DRV_STREET,
                    Suite = driver.DRV_SUITE,
                    City = driver.DRV_CITY,
                    State = driver.DRV_STATE,
                    Zip = driver.DRV_ZIP,
                    PhoneNumber = driver.DRV_CONTACT_PHONE,
                    DriverMobileNumber = driver.DRV_MOBILE_NUMBER,
                    DriverNumber = driver.DRV_NUMBER
                };

                var driverCompletedOrders = (from ce in completeEvents
                                                where ce.driverId == statement.DriverId
                                                select ce.OrderNumber)
                                                .Distinct()
                                                .ToList();
                #region for each completed order
                foreach (var item in from o in ipdEntities.ORDs
                                     join c in ipdEntities.CUSTOMERs on o.ORD_CUS_ID equals c.CUS_ID                              
                                     where statuses.Contains(o.ORD_STATUS)
                                      && driverCompletedOrders.Contains(o.ORD_NUMBER)
                                     select new
                                     {
                                         o.ORD_DRV_ID,
                                         o.ORD_NUMBER,
                                         c.CUS_BUSINESS_NAME,
                                         o.ORD_CREATE_TIME,
                                         o.ORD_CUSTOMER_REFERENCE,
                                         o.ORD_STATUS,
                                         o.ORD_TOTAL_DUE,
                                         TotalRate = o.ORD_PAY_PRICES // adj.TotalRate,
                                     })
                {
                    
                    var payableOrder = new PayableOrder
                    {
                        DriverId = item.ORD_DRV_ID ?? Guid.Empty,
                        OrderNumber = item.ORD_NUMBER,
                        ShipperName = item.CUS_BUSINESS_NAME,
                        ReferenceNumber = item.ORD_CUSTOMER_REFERENCE,
                        OfferAmount = item.TotalRate ?? 0M,
                    };

                    var stops = (from o in ipdEntities.ORDs
                                 join s in ipdEntities.STOPs on o.ORD_ID equals s.STOP_ORD_ID
                                 where o.ORD_NUMBER == payableOrder.OrderNumber
                                 select new
                                 {
                                     s.STOP_SEQ_NUMBER,                                     
                                     s.STOP_STREET,
                                     s.STOP_CITY,
                                     s.STOP_STATE,
                                     s.STOP_ZIP
                                 })
                                     .OrderBy(s => s.STOP_SEQ_NUMBER)
                                     .ToList();

                    var completedEvent = completeEvents.Where(po => po.OrderNumber == payableOrder.OrderNumber).FirstOrDefault();
                    if (completedEvent != null)
                        payableOrder.OrderCompletionDate = completedEvent.CompletionTime.ToLocalTime().Date;


                    var firstStop = stops.FirstOrDefault();
                    if (firstStop != null)
                        payableOrder.FirstStopAddress = string.Format("{0} {1}", firstStop.STOP_STREET, firstStop.STOP_ZIP);
                    var lastStop = stops.LastOrDefault();
                    if (lastStop != null)
                        payableOrder.LastStopAddress = string.Format("{0} {1}", lastStop.STOP_STREET, lastStop.STOP_ZIP);
                    
                    //Round the fee up to the nearest penny
                    payableOrder.FeeAmount = Math.Ceiling(((item.TotalRate ?? 0M) * .015M) * 100) / 100M;
                    if (payableOrder.FeeAmount < 1.5M) payableOrder.FeeAmount = 1.5M;
                    payableOrder.NetPayableAmount = payableOrder.OfferAmount - payableOrder.FeeAmount;

                    statement.PayableOrders.Add(payableOrder);
                }
                #endregion

                if (driverNumbers.Contains(statement.DriverNumber))
                    statement.PayableOrders.AddRange(additionalPOs.Where(po => po.DriverNumber == statement.DriverNumber));

                if (statement.PayableOrders.Count > 0)
                {
                    statement.TotalAmountEarned = statement.PayableOrders.Sum(po => po.OfferAmount);
                    statement.TotalIPDFees = statement.PayableOrders.Sum(po => po.FeeAmount);
                    statement.NetPayableAmount = statement.PayableOrders.Sum(po => po.NetPayableAmount);

                    rtn.Add(statement);
                }
            }

            return rtn;
        }
        

        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
        public static byte[] HexStringToByteArray(string hex)
        {
            if (string.IsNullOrEmpty(hex))
                return new byte[0];

            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
    }
}
