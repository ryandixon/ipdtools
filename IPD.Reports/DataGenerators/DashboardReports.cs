﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using IPD.Data.Oracle;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using MongoDB.Bson;
using IPD.Data.Mongo;
using System.Globalization;

using IPD.Reports.DataSetTypes;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization;

namespace IPD.Reports.DataGenerators
{
    public class DashboardReports
    {
        public static Tuple<List<String>, List<int[]>> GetIPDDailyActivity(DateTime endDate, int numberOfDays)
        {
            var completeStatuses = new string[] { "DELIVERYCOMPLETED", "PAYMENT_COMPLETE", "PAYMENT_PROCESSING" };
            var xVals = new List<string>();
            var yVals = new List<int[]>();
            var dayShortNames = new DateTimeFormatInfo().AbbreviatedDayNames;
            var mongoDb = GetMongoDBIPD();

            //Get midnight of next day for date provided
            var workingDate = new DateTime(endDate.Year, endDate.Month, endDate.Day).AddDays(1).AddDays(0 - numberOfDays);
            //for (var i = 0; i < numberOfDays; i++ )
            for (var i = 0; i < numberOfDays; i++)
            {
                var fromDate = new BsonDateTime(workingDate);
                var toDate = new BsonDateTime(workingDate.AddDays(1));
                xVals.Add(string.Format("{0} {1}", dayShortNames[(int)workingDate.DayOfWeek], workingDate.ToString(@"M/d")));

                //NOTE: as of 2/24/2015, the c# Mongo driver does not support the group operator. The pulls them all client side, cycles the enumeration a single time and creates the totals
                int totalComplete, totalOpen, totalCancelled, totalInProgress, totalOfferred;
                totalComplete = totalOpen = totalCancelled = totalInProgress = totalOfferred = 0;                
                foreach(var status in (from o in mongoDb.GetCollection<Order>("Order").AsQueryable<Order>()
                                          where o.CreateTime >= fromDate
                                              && o.CreateTime < toDate
                                          select o.Status).AsEnumerable())
                {
                    switch(status)
                    {
                        case "OPEN": totalOpen++; break;
                        case "SENDORDER": totalOfferred++; break;
                        case "ABORTED":
                        case "CANCEL": totalCancelled++; break;
                        case "DELIVERYCOMPLETED": 
                        case "PAYMENT_COMPLETE": 
                        case "PAYMENT_PROCESSING":
                            totalComplete++; break;
                        default:
                            totalInProgress++; break;
                    }
                }
                yVals.Add(new int[] { totalOpen, totalOfferred, totalInProgress, totalComplete, totalCancelled });
                
                workingDate = workingDate.AddDays(1);
            }

            //rtn.XValues = xVals.Cast<object>().ToArray();
            //rtn.YValues = yVals.Cast<object>().ToArray();
            return new Tuple<List<String>, List<int[]>>(xVals, yVals);
        }
    
        public static List<Order> GetRecentOrders(int limit = 60)
        {
            var mongodb = GetMongoDBIPD();

            var orders = (from o in mongodb.GetCollection<Order>("Order").AsQueryable<Order>()
                          orderby o.CreateTime descending
                          select o)
                         .Take(limit)
                         .ToList();

            using (var oracle = new Entities())
            {
                foreach(var order in orders)
                {
                    order.CustomerName = (from c in oracle.CUSTOMERs
                                       where c.CUS_ID == order.CustomerId
                                       select c.CUS_BUSINESS_NAME).FirstOrDefault();

                    if (order.DriverId != null && order.DriverId != Guid.Empty)
                    {
                        var driverItem = (from d in oracle.DRIVERs
                                             where d.DRV_ID == order.DriverId
                                             select new {
                                                 d.DRV_NAME,
                                                 d.DRV_MOBILE_NUMBER
                                             }
                                            ).FirstOrDefault();

                        order.DriverName = driverItem.DRV_NAME.Replace(",", " ");
                        order.DriverMobileNumber = driverItem.DRV_MOBILE_NUMBER;

                    }
                    //order.MostRecentEvent = mongodb.GetCollection<Event>("Event").AsQueryable<Event>()
                    //    .Where(e => e.strEventOrdId == order.strOrdId)
                    //    .OrderByDescending(e => e.serverTimestamp)
                    //    .Take(1)
                    //    .FirstOrDefault();
                }
            }

            return orders;
        }

        private static MongoDatabase GetMongoDBIPD()
        {
            BsonDefaults.GuidRepresentation = GuidRepresentation.Standard;
            var mongoCxnString = System.Configuration.ConfigurationManager.ConnectionStrings["mongo"].ConnectionString;
            var mongoClient = new MongoClient(mongoCxnString);
            var mongoUrl = new MongoUrl(mongoCxnString);
            return mongoClient.GetServer().GetDatabase(mongoUrl.DatabaseName);
        }
    }

    public class BarGraph
    {
        public BarGraph()
        { 
        }
        public BarGraph(string chartKey)
        {
            this.ChartKey = chartKey;
        }

        public object Chart { get; set; }
        public string ChartKey { get; private set; }
        public Func<BarGraph, object> GetChart { get; set; }
        public bool ShouldAddToCache { get; set; }
        //public List<dynamic> SeriesList { get; set; }
        //public IEnumerable<object> XValues {get; set;}
        //public IEnumerable<object> YValues {get; set;}
    }
}
