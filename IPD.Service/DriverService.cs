﻿using System.Collections.Generic;
using System.Linq;

using MongoDB.Driver;
using MongoDB.Driver.Linq;
using IPD.Data.Mongo;

using IPD.Data.Oracle;
using System;
namespace IPD.Service
{
    public class DriverService
    {
        
        public DriverGps GetDriverCurrentGPSStatus (MongoDatabase mongoDb, string mobileNumber)
        {
            return (from d in mongoDb.GetCollection<DriverGps>("DriverGps").AsQueryable()
                            where d.MobileNumber == mobileNumber
                            select d)
                            .FirstOrDefault();
        }

        public List<GpsData> GetDriverLocationHistory(MongoDatabase mongoDb, string strGpsDrvId, int limit = 50, int skip=0)
        {
            return (from gps in mongoDb.GetCollection<GpsData>("GpsData").AsQueryable()
                    where gps.strGpsDrvId == strGpsDrvId
                    select gps)
                    .OrderByDescending(gps => gps.TimeStamp)
                    .Skip(skip)
                    .Take(limit)
                    .ToList();
        }

        public DRIVER GetDriverInfo(Entities ipdEntities, string mobileNumber)
        {
            return (from d in ipdEntities.DRIVERs
                    where d.DRV_MOBILE_NUMBER == mobileNumber
                    select d).FirstOrDefault();
        }

        public List<DriverInMessage> GetDriverInMessage(MongoDatabase mongoDb, string strDrvmsgDrvId, int limit = 50, int skip = 0)
        {
            return (from inMsg in mongoDb.GetCollection<DriverInMessage>("DriverInMessage").AsQueryable()
                    where inMsg.strDrvmsgDrvId == strDrvmsgDrvId
                    select inMsg)
                    .OrderByDescending(msg => msg.ReceivedTime)
                    .Skip(skip)
                    .Take(limit)
                    .ToList();
        }

        public IQueryable<DriverSearchResult> SearchForDriver(Entities ipdEntities, string pTerm)
        {
            var term = System.Text.RegularExpressions.Regex.Replace(pTerm.Trim(), "[ ()-.]", "");
            if (term.Length < 3)
                return null;

            int outInt;
            if (int.TryParse(term, out outInt))
            {
                //search by mobilenumber
                return (from d in ipdEntities.DRIVERs
                           where d.DRV_MOBILE_NUMBER.StartsWith(term)
                           select new DriverSearchResult
                           {
                               id = d.DRV_ID,
                               MobileNumber = d.DRV_MOBILE_NUMBER,
                               Name = d.DRV_NAME
                           })
                           .OrderBy(d => d.MobileNumber);
            }
            else
            {
                //search by name
                return (from d in ipdEntities.DRIVERs
                        where d.DRV_NAME.ToLower().Contains(term.ToLower())
                        select new DriverSearchResult
                        {
                            id = d.DRV_ID,
                            MobileNumber = d.DRV_MOBILE_NUMBER,
                            Name = d.DRV_NAME
                        })
                        .OrderBy(d => d.Name);
            }
        }
    }
    public class DriverSearchResult
    {
        public Guid id { get; set; }
        public string MobileNumber { get; set; }
        public string Name { get; set; }
    }
}
