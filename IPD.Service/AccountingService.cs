﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IPD.Data;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using IPD.Data.Mongo;

using IPD.Data.Accounting;
using System.Data.Entity.SqlServer;


namespace IPD.Service
{
    public class AccountingService
    {
        public DateTimeOffset GetWeeklyCutoffDate(DateTime asOfDate)
        {
            DateTimeOffset utcCutoffDateTime;
            var cutoffDateTime = asOfDate;
            while ((int)cutoffDateTime.DayOfWeek > 0)
                cutoffDateTime = cutoffDateTime.AddDays(-1);
            //var cutoffDateTime = asOfDate.AddDays(-(((int)asOfDate.DayOfWeek) + 1));
            utcCutoffDateTime = new DateTimeOffset(cutoffDateTime.Year, cutoffDateTime.Month, cutoffDateTime.Day, 0, 0, 0, new TimeSpan(0, 0, 0));

            return utcCutoffDateTime;
        }
        public AccountingBatch CreatePaymentBatch(IPDAccounting ipdAccountingContext, string userName, AccountingBatch.AccountingBatchTypes batchType, DateTime asOfDate, string description = null)
        {
            var batch = AccountingBatch.Create(userName, batchType);
            batch.AsOfDateTime = asOfDate;
            if (description != null)
                batch.Description = description;

            var mongoDb = IPDMongo.GetIPDDb(System.Configuration.ConfigurationManager.ConnectionStrings["mongo"].ConnectionString);

            batch.CutoffDateTime = GetWeeklyCutoffDate(asOfDate).DateTime;
            var bsonDate = BsonDateTime.Create(batch.CutoffDateTime);
            foreach (var batchOrder in (from o in mongoDb.GetCollection<IPD.Data.Mongo.Order>("Order").AsQueryable<IPD.Data.Mongo.Order>()
                                  where o.CompletionTime <= bsonDate
                                    && o.Status == "DELIVERYCOMPLETED"
                                    && o.strOrdId.Length ==32
                                  select new OrderAccountingBatch
                                  {
                                      BatchId = batch.id,
                                      AccountingBatch = batch,
                                      UpdatedBy = userName,                                      
                                      OrderId = new Guid(IPDMongo.HexStringToByteArray(o.strOrdId)),
                                      Status = OrderAccountingBatch.OrderAccountingBatchStatuses.New,
                                      CreatedTime = DateTime.Now
                                  }))
            {
                //
                //TODO: Add logic to exclude orders with an exiting new or pending payment batch
                //
                batchOrder.id = Guid.NewGuid();
                batch.OrderAccountingBatches.Add(batchOrder);
            }

            return batch;
        }

        public AccountingBatch CreateCollectionBatch(IPDAccounting ipdAccountingContext, string userName, DateTime asOfDate, string description = null)
        {
            var batch = AccountingBatch.Create(userName, AccountingBatch.AccountingBatchTypes.Collection);
            batch.AsOfDateTime = asOfDate;
            if (description != null)
                batch.Description = description;

            var mongoDb = IPDMongo.GetIPDDb(System.Configuration.ConfigurationManager.ConnectionStrings["mongo"].ConnectionString);

            batch.CutoffDateTime = GetWeeklyCutoffDate(asOfDate).DateTime;
            var bsonDate = BsonDateTime.Create(batch.CutoffDateTime);

            var minDate = new DateTime(2015,4,2);
            var statuses = new string[] {"DELIVERYCOMPLETED","PAYMENT_COMPLETE"};
            //First get orders that are completed but have not collected pay_status
            foreach( var batchOrder in (from o in ipdAccountingContext.Orders
                                        where statuses.Contains(o.Status)
                                                && o.ORD_PAY_STATUS == null
                                                //Why this date limit? Because prior to this date there are some orders with Guids that are not 32 bytes long, causing major problems for EF Guid mapping
                                                && o.CreateTime > minDate
                                                //&& o.id.ToByteArray().Length == 16
                                                select
                                                  new
                                                  {
                                                      o.id
                                                  })
                                                .ToList()
                                                .Select(o => 
                                        new OrderAccountingBatch
                                        {
                                            BatchId = batch.id,
                                            AccountingBatch = batch,
                                            UpdatedBy = userName,
                                            OrderId = o.id,
                                            Status = OrderAccountingBatch.OrderAccountingBatchStatuses.New,
                                            CreatedTime = DateTime.Now
                                        }))
            {
                batchOrder.id = Guid.NewGuid();
                batch.OrderAccountingBatches.Add(batchOrder);

            }
            return batch;
        }

        public IQueryable<AccountingBatch> GetAccountingBatches(IPDAccounting ipdAccountingContext, AccountingBatch.AccountingBatchTypes[] pTypes, AccountingBatch.AccountingBatchStatuses[] pStatuses = null)
        {
            var statusList = pStatuses.Select(s => s.ToString()).ToArray()
                ??  new string[] { 
                        AccountingBatch.AccountingBatchStatuses.New.ToString(), 
                        AccountingBatch.AccountingBatchStatuses.Processing.ToString() 
                    };
            var types = (from t in pTypes 
                        select t.ToString())
                        .ToList();
            return (from ab in ipdAccountingContext.AccountingBatches
                       where types.Contains(ab.Type)
                       && statusList.Contains(ab.StatusString)
                       select ab)
                       .OrderByDescending( x => x.CreatedDateTime)
                       .Take(50);
        }

        public List<OrderAccountingBatch> GetBatchOrders(IPDAccounting ipdAccountingContext, Guid batchId)
        {
            return (from oab in ipdAccountingContext.OrderAccountingBatches
                       join o in ipdAccountingContext.Orders on oab.OrderId equals o.id
                       join d in ipdAccountingContext.Drivers on o.DriverId equals d.id
                       join c in ipdAccountingContext.Customers on o.CustomerId equals c.id
                       where oab.BatchId == batchId
                       select new
                       {
                           oab,
                           o.TotalDue,
                           o.OrderNumber,
                           d.Name,
                           d.MobileNumber,
                           IsApprovedForPmt = d.IsApprovedForPmt == "Y",
                           c.BusinessName,
                           o.ORD_COMPLETE_TIME
                       })
                   .ToList()
                   .Select(item => new OrderAccountingBatch
                   {
                       id = item.oab.id,
                       OrderId = item.oab.OrderId,
                       OrderNumber = item.OrderNumber,
                       Status = item.oab.Status,
                       DriverName = item.Name,
                       DriverMobileNumber = item.MobileNumber,
                       CustomerName = item.BusinessName,
                       TotalDue = item.TotalDue,
                       CompletedDate = item.ORD_COMPLETE_TIME == null ? DateTime.MinValue : item.ORD_COMPLETE_TIME.Value,
                       IsApprovedForPmt = item.IsApprovedForPmt
                   })
                   .ToList();
            
        }
    
        public AccountingBatch AdvanceBatchStatus(IPDAccounting ipdAccountingContext, MongoDatabase mongoDb, Guid batchId)
        {
            if (mongoDb == null)
                mongoDb = IPD.Data.Mongo.IPDMongo.GetIPDDb(System.Configuration.ConfigurationManager.ConnectionStrings["mongo"].ConnectionString);

            var accountingBatch = (from ab in ipdAccountingContext.AccountingBatches.Include("OrderAccountingBatches")
                                   where ab.id == batchId
                                   select ab)
                                       .FirstOrDefault();

            if (accountingBatch != null)
            {
                //This needs to be coordinated using MSFT Distributed Transaction Service so the Oracle and Mongo actions happen atomically (as a single transaction)
                accountingBatch.AdvanceStatus();
                accountingBatch.UpdatedBy = "unknownUser";
                ipdAccountingContext.SaveChanges();

                //If we moved to Final
                if (accountingBatch.Status == AccountingBatch.AccountingBatchStatuses.Final)
                {
                    if (accountingBatch.Type == "Payment")
                    {
                        var sql = String.Format(
    @"UPDATE ORD
    set ORD_STATUS = 'PAYMENT_COMPLETE'
    where ORD_ID in (select ORDERID from ORDER_ACCOUNTINGBATCH where BATCHID = '{0}'
                        and STATUS = 'Processing')", accountingBatch.id.ToOracleGuid().ToOracleString());
                        ipdAccountingContext.Database.ExecuteSqlCommand(sql);
                        ipdAccountingContext.SaveChanges();
                
                        var orderIds = accountingBatch.OrderAccountingBatches.Select(oab => oab.OrderId.ToOracleGuid().ToOracleString()).ToList();
                        var ordersCollection = mongoDb.GetCollection<IPD.Data.Mongo.Order>("Order");
                        var query = Query<IPD.Data.Mongo.Order>.In(o => o.strOrdId, orderIds);
                        var set = Update<IPD.Data.Mongo.Order>.Set(o => o.Status, "PAYMENT_COMPLETE");
                        ordersCollection.Update(query, set, UpdateFlags.Multi);
                    }
                    else if (accountingBatch.Type == "Collection")
                    {
                        //update the PAY_STATUS Field
                        var sql = String.Format(
    @"UPDATE ORD
    set ORD_PAY_STATUS = 'Billed'
    where ORD_ID in (select ORDERID from ORDER_ACCOUNTINGBATCH where BATCHID = '{0}'
                        and STATUS = 'Success')", accountingBatch.id.ToOracleGuid().ToOracleString());
                        ipdAccountingContext.Database.ExecuteSqlCommand(sql);
                        ipdAccountingContext.SaveChanges();

                    }

                }

            }

            return accountingBatch;
        }
    
        public AccountingBatch CancelBatch(IPDAccounting ipdAccountingContext, Guid batchId)
        {
            var accountingBatch = (from ab in ipdAccountingContext.AccountingBatches.Include("OrderAccountingBatches")
                                   where ab.id == batchId
                                   select ab)
                                   .First();

            accountingBatch.Cancel();

            return accountingBatch;

        }
    
        public bool DeleteBatch(IPDAccounting ipdAccountingContext, Guid batchId)
        {
            var rtn = false;
            var batch = ipdAccountingContext.AccountingBatches.Include("OrderAccountingBatches").Where(b => b.id == batchId).First();
            if (batch.Status == AccountingBatch.AccountingBatchStatuses.New)
            {
                ipdAccountingContext.OrderAccountingBatches.RemoveRange(batch.OrderAccountingBatches);
                ipdAccountingContext.AccountingBatches.Remove(batch);
                rtn = true;
            }

            return rtn;
            
        }
    }
}
