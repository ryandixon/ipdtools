﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Accounting
{
    [MetadataType(typeof(AccountingBatchAttributes))]
    public partial class AccountingBatch
    {
        public enum AccountingBatchTypes { Collection, Payment }
        public enum AccountingBatchStatuses { New, Processing, Final, Canceled }
        public static AccountingBatchStatuses? GetNextStatus (string currentStatus)
        {
            return GetNextStatus((AccountingBatchStatuses)Enum.Parse(typeof(AccountingBatchStatuses), currentStatus));
        }
        public static AccountingBatchStatuses GetNextStatus (AccountingBatchStatuses currentStatus)
        {
            return currentStatus == AccountingBatch.AccountingBatchStatuses.New ? AccountingBatch.AccountingBatchStatuses.Processing
                                : currentStatus == AccountingBatch.AccountingBatchStatuses.Processing ? AccountingBatch.AccountingBatchStatuses.Final
                                : currentStatus == AccountingBatch.AccountingBatchStatuses.Canceled ? AccountingBatch.AccountingBatchStatuses.Canceled
                                : AccountingBatchStatuses.Final;
        }

        public AccountingBatchStatuses Status
        {
            get { return (AccountingBatchStatuses)Enum.Parse(typeof(AccountingBatchStatuses), (this.StatusString ?? AccountingBatchStatuses.New.ToString())); }
            set { this.StatusString = value.ToString(); }
        }

        public static AccountingBatch Create(string userName, AccountingBatchTypes batchType)
        {
            return new AccountingBatch()
            {                
                UpdatedBy = userName,
                id = Guid.NewGuid(),
                Type = batchType.ToString(),
                Description = String.Format("Batch {0}", DateTime.Now.ToString("yy-MMM-dd h:mm tt")),
                CreatedDateTime = DateTime.Now,
                Status = AccountingBatchStatuses.New
            };    
        }

        public void AdvanceStatus()
        {
            var nextStep = AccountingBatch.GetNextStatus(this.Status);
            if (nextStep != this.Status)
            {
                foreach (var order in this.OrderAccountingBatches)
                {
                    order.UpdateStatus(nextStep);
                }
                this.Status = nextStep;
            }
        }

        public bool Cancel()
        {
            var success = false;
            if (this.Status != AccountingBatchStatuses.Final && this.Status != AccountingBatchStatuses.Canceled)
            {
                success = true;
                foreach (var order in this.OrderAccountingBatches)
                {
                    success = order.UpdateStatus(AccountingBatchStatuses.Canceled);
                    if (!success)
                        break;
                }

                if (success)
                    this.Status = AccountingBatchStatuses.Canceled; ;
            }
            return success;
        }
    }

    public class AccountingBatchAttributes
    {
        [Display(Name = "Description")]
        public string Description {get; set;}

        [Display(Name = "Batch Status")]
        public string Status {get; set;}

        [Display(Name = "Last Modified By")]
        public string UpdatedBy { get; set; }
    }

    public partial class OrderAccountingBatch
    {
        public enum OrderAccountingBatchStatuses { New, Processing, Failed, Success, Returned, Canceled }

        /// <summary>
        /// Advances this based on currentBatchStatus
        /// </summary>
        /// <param name="currentBatchStatus">The status that the batch is moving to (not from) that this order should use to advance itself.</param>
        public bool UpdateStatus(AccountingBatch.AccountingBatchStatuses currentBatchStatus)
        {
            switch (currentBatchStatus)
            {
                case Accounting.AccountingBatch.AccountingBatchStatuses.Processing: this.Status = OrderAccountingBatchStatuses.Processing; break;
                case Accounting.AccountingBatch.AccountingBatchStatuses.Final: 
                    if (this.Status != OrderAccountingBatchStatuses.Failed || this.Status != OrderAccountingBatchStatuses.Returned)
                        this.Status = OrderAccountingBatchStatuses.Success; 
                    break;
                case Accounting.AccountingBatch.AccountingBatchStatuses.Canceled:
                    if (this.Status == OrderAccountingBatchStatuses.Success) 
                        return false;
                    else
                        this.Status = OrderAccountingBatchStatuses.Canceled;
                    break;
            }

            return true;
        }

        public OrderAccountingBatchStatuses Status
        {
            get { return (OrderAccountingBatchStatuses)Enum.Parse(typeof(OrderAccountingBatchStatuses), (this.StatusString ?? OrderAccountingBatchStatuses.New.ToString())); }
            set { this.StatusString = value.ToString(); }
        }
        //Non-store members 
        public DateTime CompletedDate { get; set; }
        public string CustomerName { get; set; }
        public string DriverName { get; set; }
        public string DriverMobileNumber { get; set; }
        public bool IsApprovedForPmt { get; set; }
        public string OrderNumber { get; set; }
        public decimal? TotalDue { get; set; }
    }
}
