CREATE TABLE "IPD"."ACCOUNTINGBATCH" 
(	
	"ID" RAW(16) NOT NULL ENABLE,
	"TYPE" VARCHAR2(32 BYTE) NOT NULL ENABLE,
	"DESCRIPTION" VARCHAR2(64 BYTE) NOT NULL ENABLE,
	"STATUS" VARCHAR2(32 BYTE) NOT NULL ENABLE, 
	"CUTOFFDATETIME" DATE,
	"ASOFDATETIME" DATE,
	"MESSAGEHEADER" VARCHAR(255),
	"MESSAGE" VARCHAR(4000),
	"CREATEDATETIME" DATE,
	"UPDATEDBY" VARCHAR2(32 BYTE) NOT NULL ENABLE, 
	CONSTRAINT accountingbatch_pk PRIMARY KEY ("ID")     
);

CREATE TABLE "IPD"."ORDER_ACCOUNTINGBATCH" 
     
(	
	"ID" RAW(16) NOT NULL ENABLE, 

        "ORDERID" RAW(16) NOT NULL ENABLE,
	"BATCHID" RAW(16) NOT NULL ENABLE,
	"STATUS" VARCHAR2(32 BYTE) NOT NULL ENABLE, --NEW, PROCESSING, FAILED, SUCCESS, RETURNED -- We need this to handle individual payments that fail for whatever reason
          
	"CREATEDATETIME" DATE,
	"UPDATEDBY" VARCHAR2(32 BYTE) NOT NULL ENABLE, 
	CONSTRAINT order_accountingbatch_pk PRIMARY KEY ("ID")

);


CREATE TABLE "IPD"."CUSTOMER_INVOICE"      
(	
	"ID" RAW(16) NOT NULL, 
	"NUMBER" varchar(16) NOT NULL, 
	"BATCHID" RAW(16) NOT NULL,     
	"PREVIOUS_BALANCE" NUMBER(24) NULL,
	"TOTALDUE" NUMBER(24) NULL,	
	"AMOUNTPAID" NUMBER(24) NULL,
	"CREATEDATETIME" DATE,
	"UPDATEDBY" VARCHAR2(32 BYTE) NOT NULL, 
	CONSTRAINT CUSTOMER_INVOICE_pk PRIMARY KEY ("ID")
);


ALTER TABLE DRIVER
ADD (	APPROVED_FOR_PMT CHAR(1) DEFAULT 'N',
        ACH_PAYEEID VARCHAR2(64) NULL,
        PREFERRED_PMT_METHOD VARCHAR(64) NULL,
	DRV_BADGEID VARCHAR(24) NULL);


ALTER TABLE DRIVER
ADD (	SYNC_HASH VARCHAR2(768),
	LAST_SYNC_DATE DATE NULL,
	SYNC_HASH_DATE DATE NULL);





CREATE OR REPLACE TRIGGER Driver_INSERT_UPDATE
  BEFORE INSERT OR UPDATE  ON DRIVER
  FOR EACH ROW
DECLARE
    hashVal varchar(2048);
    delimiter varchar(2);
    nameValue varchar(64);
BEGIN
        delimiter := '||';
        
        --since there is not always a comma present, make sure we add one, if it is missing
        if (INSTR(:new.DRV_Name, ',') = 0) THEN
          nameValue := nameValue || ','; -- no comma essentially means the entire name field is the first name (though not really true if you look at the data)
        else  
          nameValue := :new.DRV_NAME;
        END IF;
        
        hashVal := 
            :new.DRV_BADGEID
            || delimiter  || :new.DRV_MOBILE_NUMBER
            || delimiter || REPLACE(:new.DRV_Name, ',', delimiter) 
            || delimiter || :new.DRV_EMAIL 
            || delimiter || :new.DRV_STREET
            || delimiter || :new.DRV_SUITE
            || delimiter || :new.DRV_CITY
            || delimiter  || :new.DRV_STATE
            || delimiter  || :new.DRV_ZIP
            || delimiter  || :new.DRV_COUNTRY
            || delimiter  || :new.DRV_BUSINESS_NAME
            || delimiter  || :new.DRV_ACTIVE_STATUS
            || delimiter  || :new.ACH_PAYEEID
            || delimiter  || :new.APPROVED_FOR_PMT;
        if (hashVal != NVL(:old.SYNC_HASH,'')) THEN
          :new.SYNC_HASH := hashVal;
          :new.SYNC_HASH_DATE := SYS_EXTRACT_UTC(SYSTIMESTAMP);
        END IF;
END;