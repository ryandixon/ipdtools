﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Document
{
    public class RawMessage
    {
        public Guid Id { get; set; }
        public RawMessageDirection Direction {get; set;}
        public Guid DeviceId { get; set; }
        public DateTimeOffset CreateTime { get; set; }
        public DateTimeOffset ProcessCompletionTime { get; set; }
        public string Body { get; set; }
    }

    public enum RawMessageDirection {Inbound, Outbound}

    /*
     * Outboud message types
     * 
     * New Order
     * Order Update
     * New Offer 
     * Offer Update
     * New Chat Message (from dispatcher)
     * 
     */

    /*
     * Inboud message types
     * 
     * Offer response (bid/decline)
     * POD Update
     * Inbound Message Ack
     * New Chat Message (from driver)
     * 
     * 
     */
}
