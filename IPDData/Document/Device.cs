﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Document
{
    public class Device
    {
        public Guid Id { get; set; }
        public Guid BusinessEntityId { get; set; }
        public Guid CurrentAppVersion { get; set; }
        public string Model { get; set; }

    }
}
