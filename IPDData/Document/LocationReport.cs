﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Document
{
    public class LocationReport
    {
        public string MobileNumber { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
        /// <summary>
        /// Accuracy in Meters of location
        /// </summary>
        public decimal Accuracy { get; set; }

        /// <summary>
        /// Current bearing, in degrees 0 - 360.
        /// </summary>
        public decimal Bearing { get; set; }

        /// <summary>
        /// Speed of travel in meters/second
        /// </summary>
        public decimal Speed { get; set; }
    }
}
