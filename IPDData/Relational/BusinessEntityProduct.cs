﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Relational
{
    /// <summary>
    /// Holds a business entites special pricing for a specific product. When a record exists, the values here override the values from the Product Table
    /// </summary>
    public class BusinessEntityProduct
    {
        public Guid BusinessEntityId { get; set; }
        public Guid ProductId { get; set; }

        public string Name { get; set; }
        /// <summary>
        /// When not null, this value is is multiplied by the value specified in the RateCostsBasis property to determine the product's cost
        /// </summary>
        public decimal? RateCost { get; set; }

        /// <summary>
        /// When RateCost is specified, this is the minimum value that should be the cost of the item
        /// </summary>
        public decimal? MinimumCost { get; set; }

        public decimal? FixedCost { get; set; }

        public DateTimeOffset CreateTime { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset LastUpdatedTime { get; set; }
        public string LastUpdatedBy { get; set; }
    }
}
