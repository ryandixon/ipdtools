﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Relational
{
    public class RateSchedule
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class RateTier
    {
        public int Id { get; set; }
        public int MinimumOrderVolumne {get; set;}
        public decimal OrderMinimumFee { get; set; }
        public decimal Rate { get; set; }
    }


}
