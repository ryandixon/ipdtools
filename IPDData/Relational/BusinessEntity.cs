﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Relational
{
    /// <summary>
    /// Any contractor or business that we might pay or receive money from.
    /// </summary>
    public class BusinessEntity
    {
        public Guid Id { get; set; }
        public Guid ParentBusinessEntityId { get; set; }
        public string OrganizationName { get; set; }
        public Guid? PrimaryContactId { get; set; }
        public Guid? BillingAddressId { get; set; }
        public Guid? PhysicalAddressId { get; set; }
        public Guid? IPDAccountId { get; set; }

        //This should be handled in an accounting system!!
        //public bool ApprovedForPayment { get; set; }
        //public string PreferredPaymentMethod { get; set; }

        //public bool ApprovedForCollection { get; set; }
        //public string PreferredCollectionMethod { get; set; }
    }

}
