﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Relational
{
    public class OrderAccessorial
    {
        public Guid OrderId { get; set; }
        public string AccessorialId { get; set; }
        public bool Optional { get; set; }
    }
}
