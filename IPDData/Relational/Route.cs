﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Relational
{
    /// <summary>
    /// Holds a recuring route definition. Info herein will be used to create a separate order for each execution of the route.
    /// </summary>
    public class Route
    {
        public Guid Id { get; set; }
    }
}
