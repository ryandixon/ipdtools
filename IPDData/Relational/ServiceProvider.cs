﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Relational
{
    public class ServiceProvider
    {
        public Guid Id { get; set; }
        /// <summary>
        /// Driver number
        /// </summary>
        public string ProviderNumber { get; set; }
        public Guid BusinessEntityId { get; set; }
        public string MobileNumber { get; set; }

        /// <summary>
        /// A unique invitation code valid for a specific IC
        /// </summary>
        public string InvitationCode { get; set; }
    }
}
