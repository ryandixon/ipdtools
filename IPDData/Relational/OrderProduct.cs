﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Relational
{
    /// <summary>
    /// This contians a product associated with an order
    /// </summary>
    public class OrderProduct
    {
        public Guid OrderId { get; set; }
        public Guid ProductId { get; set; }

        /// <summary>
        /// The business Entity Id of the party purchasing this product. i.e. the originator or the service provider. 
        /// </summary>
        public Guid PurchaserId { get; set; }

        public decimal Price { get; set; }

        public int? Quantity { get; set; }

        public DateTimeOffset CreateTime { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset LastUpdatedTime { get; set; }
        public string LastUpdatedBy { get; set; }
    }
}
