﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Relational
{
    /*Examples 
     *Product: 
     *  Name: OriginatorFee, 
     *  RateCostBasis: ServiceProviderOfferAmount,
     *  Rate: .03,  
     *  MinimumCost: $1.50
     *  
     *Product: 
     *  Name: SerivceProviderFee, 
     *  RateCostBasis: ServiceProviderOfferAmount,
     *  Rate: .03,  
     *  MinimumCost: $1.50
     * 
     *Product: 
     *  Name: OriginatorFee, 
     *  RateCostBasis: DeclaredValue,
     *  Rate: .1,  
     *  MinimumCost: $2.00
     *  
     */
    /// <summary>
    /// Represents a product sold by IPD. Ex: transaction fee, driver insurace, cargo insurance, etc. 
    /// </summary>
    public class Product
    {
        public Guid Id { get; set; }

        /// <summary>
        /// A unique product code (that is easier to read than a GUID!)
        /// </summary>
        public string Code { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        /// <summary>
        /// The value to which the cost rate should be applied. 
        /// </summary>
        public RateCostBasis RateCostsBasis { get; set; }

        /// <summary>
        /// When not null, this value is is multiplied by the value specified in the RateCostsBasis property to determine the product's cost
        /// </summary>
        public decimal? RateCost { get; set; }

        /// <summary>
        /// When RateCost is specified, this is the minimum value that should be the cost of the item
        /// </summary>
        public decimal? MinimumCost { get; set; }

        public decimal? FixedCost { get; set; }

        public DateTimeOffset CreateTime { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset LastUpdatedTime { get; set; }
        public string LastUpdatedBy { get; set; }

    }

    public enum RateCostBasis { 
        ServiceProviderOfferAmount, 
        GrossOriginatorAmountDue, //Excludes rate based items
        DeclaredValue //Order Declared Value of goods
    }
}
