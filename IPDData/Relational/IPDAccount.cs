﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Relational
{
    public class IPDAccount
    {
        public Guid Id { get; set; }
        public string AccountNumber { get; set; }
        public Guid BusinessEntityId { get; set; }
        public string ExternalId { get; set; }
    }
}
