﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Relational
{
    public class ExternalOrderId
    {
        public Guid OrderId { get; set; }
        public ExternalOrderIdSource Source { get; set; }
        /// <summary>
        /// A descriptor of the External Id. Generally provided by the externally entity - e.g. Case Number, COPS Order Id, Agent's Customer's Account Id
        /// </summary>
        public string IdType { get; set; }
        public string ExternalId { get; set; }
    }

    public enum ExternalOrderIdSource { Agent, Shipper }
}
