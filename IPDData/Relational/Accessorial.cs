﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Relational
{
    public class Accessorial
    {
        /// <summary>
        /// A code stipulating the accessorial
        /// </summary>
        public string Id { get; set; }
        public string Description { get; set; }
    }
}
