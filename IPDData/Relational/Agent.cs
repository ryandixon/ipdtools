﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Relational
{
    public class Agent
    {
        public Guid Id { get; set; }
        public string AgentNumber { get; set; }
        public Guid BusinessEntityId { get; set; }
        
    }
}
