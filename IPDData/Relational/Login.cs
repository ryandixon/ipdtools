﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Relational
{
    public class Login
    {
        public Guid Id { get; set; }
        /// <summary>
        /// The business entity associated with this login - Agent, Shipper, IPD User, etc.
        /// </summary>
        public Guid BusinessEntityId { get; set; }
        public string UserName { get; set; }
        public Guid ContactId { get; set; }

        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public string PasswordQuestion { get; set; }
        public string PasswordAnswer { get; set; }
        public bool IsLocked { get; set; }

        public int FailedPasswordAttemptCount { get; set; }
        public int LastFailedPasswordAttemptTime { get; set; }

        public DateTimeOffset LastSuccessfulLoginTime { get; set; }
        public DateTimeOffset CreateTime { get; set; }
    }
}
