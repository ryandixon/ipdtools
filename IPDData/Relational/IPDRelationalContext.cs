﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace IPD.Data.Relational
{
    public class IPDRelationalContext: DbContext
    {
        public IPDRelationalContext(): 
            base ("CxnString")
        { }

        public DbSet<Address> Addresses { get; set; }
        public DbSet<Accessorial> Accessorials { get; set; }
        public DbSet<Agent> Agents { get; set; }
        public DbSet<BusinessEntity> BusinessEntities { get; set; }
        public DbSet<BusinessEntityGroup> BusinessEntityGroups { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<ExternalOrderId> ExternalOrderIds { get; set; }
        public DbSet<IPDAccount> IPDAccounts { get; set; }
        public DbSet<Login> Logins { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderAccessorial> OrderAccessorials { get; set; }
        public DbSet<OrderSecondaryParty> OrderSecondaryParties { get; set; }
        public DbSet<PerformanceRating> PerformanceRatings { get; set; }
        public DbSet<RateSchedule> RateSchedules { get; set; }
        public DbSet<Route> Routes { get; set; }
        public DbSet<ServiceProvider> ServiceProviders { get; set; }
        public DbSet<Stop> Stops { get; set; }
        public DbSet<StopTransportPiece> StopTransportItems { get; set; }
        public DbSet<TransportPiece> TransportItems { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //base.OnModelCreating(modelBuilder);
        }
    }
}
