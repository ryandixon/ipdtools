﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Relational
{
    public class StopTransportPiece
    {
        public Guid StopId { get; set; }
        public Guid TransportPieceId { get; set; }
        public int ScheduledNumberPieces { get; set; }
        public int ActualNumberPieces { get; set; }
    }
}
