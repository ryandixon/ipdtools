﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Relational
{
    public class Stop
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Id of order that this stop is included in. Allways null if RouteId is populated.
        /// </summary>
        public Guid? OrderId { get; set; }

        /// <summary>
        /// Id of predefined route. Always null if OrderId is populated.
        /// </summary>
        public Guid? RouteId { get; set; }

        public Guid AddressId { get; set; }
        public DateTimeOffset? ScheduledBeginTime { get; set; }
        public DateTimeOffset? ScheduledEndTime { get; set; }
        

        /// <summary>
        /// When true, the service provider must go inside a building to complete the stop.
        /// </summary>
        public bool BuildingEntranceRequired { get; set; }

        public string ContactName { get; set; }
        public string ContactPhoneNumber { get; set; }
        public string ContactEmail { get; set; }
        public string Note { get; set; }

        public bool SignatureRequired { get; set; }
        public string NameOfSignor { get; set; }
        public Guid? SignatureImageId { get; set; }
        public DateTimeOffset StopCompletionTime { get; set; }
        public string ServiceProviderNote { get; set; }

        public DateTimeOffset CreateTime { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset LastUpdatedTime { get; set; }
        public string LastUpdatedBy { get; set; }

    }
}
