﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Relational
{
    public class PerformanceRating
    {
        public Guid Id { get; set; }

        /// <summary>
        /// The order that is the basis for the rating
        /// </summary>
        public Guid OrderId { get; set; }

        /// <summary>
        /// BusinessEntityId of the entity that provided the rating
        /// </summary>
        public Guid Rater { get; set; }

        /// <summary>
        /// Business Entity Id of the entity being rated
        /// </summary>
        public Guid Ratee { get; set; }

        /// <summary>
        /// Rating. I.e. number of stars
        /// </summary>
        public int Rating { get; set; }

        public string Comment { get; set; }
    }
}
