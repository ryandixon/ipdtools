﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Relational
{
    public class OrderSecondaryParty
    {
        public Guid OrderId { get; set; }
        public Guid BusinessEntityId { get; set; }
        public SecondaryPartyRelationship Relationship {get; set;}
    }

    public enum SecondaryPartyRelationship 
    {
        Shipper, 
        Receiver, 
        Other //A party probably with ReadOnly needs to view the data. Gov't monitor prehaps?
    }
}
