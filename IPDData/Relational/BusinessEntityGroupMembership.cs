﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Relational
{
    /// <summary>
    /// A business entity's membership in a group defined by another business entity.  Multiple membership records for the same Member are permitted so long as they do not overlap timeperiods!!
    /// </summary>
    public class BusinessEntityGroupMembership
    {

        public Guid BusinessEntityGroupId { get; set; }

        /// <summary>
        /// The Business Entity that is a member of the BusinessEntityGroup
        /// </summary>
        public Guid MemberId { get; set; }

        /// <summary>
        /// The begin date of the membership.
        /// </summary>
        public DateTime MembershipBeginTime { get; set; }


        /// <summary>
        /// When set, the date when the members inclusion in the group expires.
        /// </summary>
        public DateTime? MembershipEndTime { get; set; }
    }}
