﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Relational
{
    public class Order
    {
        public Guid Id { get; set; }
        /// <summary>
        /// Unique number string identifying this order
        /// </summary>
        public string OrderNumber { get; set; }

        /// <summary>
        /// When not null, the guid of the route this order was created from.
        /// </summary>
        public Guid? RouteId { get; set; }

        /// <summary>
        /// The id (guid) of the BusinessEntity who originated this order. This is the entity from whom funds will be collected to for the completion of this order.
        /// </summary>
        public Guid OriginatorId { get; set; }

        /// <summary>
        /// The id (guid) of the BusinessEntity who is the service provider that will complete this order. This is the entity that will be paid.
        /// </summary>
        public Guid ServiceProviderId { get; set; }

        public OrderStatus Status { get; set; }

        public OrderPaymentStatus PaymentStatus { get; set; }
        public OrderCollectionStatus CollectionStatus { get; set; }

        /// <summary>
        /// The stated driver settlement amount. I.e. the amount agreed to by the order orginator and service provider for completion of the order
        /// </summary>
        public decimal ServiceProviderOfferAmount { get; set; }

        public int OfferExpirationSeconds { get; set; }
        public decimal DeclaredValue { get; set; }
        public bool IncludesDangerousGoods { get; set; }

        /// <summary>
        /// When true, this order should automatically be assigned to the first service provider to bid on it.
        /// </summary>
        public bool AutoAssignToFirstBidder { get; set; }


        public DateTimeOffset CreateTime { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset LastUpdatedTime { get; set; }
        public string LastUpdatedBy { get; set; }
    }

    public enum OrderStatus {New, Offered, Assigned, Started, Complete, Canceled, Aborted, Closed}

    public enum OrderPaymentStatus { Unpaid, PaymentPending, PartiallyPaid, Complete}

    public enum OrderCollectionStatus { Unbilled, Billed, PartiallyCollected, Complete}
}
