﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Relational
{
    public class BusinessEntityGroup
    {
        /// <summary>
        /// The Business Entity defining this group
        /// </summary>
        public Guid BusinessEntityId { get; set; }

        public BusinessEntityGroupType Type { get; set; }
        public string Name { get; set; }
    }

    public enum BusinessEntityGroupType { Approved, Blocked, Custom}


}
