﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Oracle
{
    public partial class DRIVER
    {
        public string ToSyncHash()
        {
            //Concat fields we care about sync'ing
            List<string> f = new List<string>();
            f.Add(this.DRV_BADGEID);
            f.Add(this.DRV_MOBILE_NUMBER);
            f.Add(this.FirstName);
            f.Add(this.LastName);
            f.Add(this.DRV_EMAIL);
            f.Add(this.DRV_STREET);
            f.Add(this.DRV_SUITE);
            f.Add(this.DRV_CITY);
            f.Add(this.DRV_STATE);
            f.Add(this.DRV_ZIP);
            f.Add(this.DRV_COUNTRY);
            f.Add(this.DRV_BUSINESS_NAME);
            f.Add(this.DRV_ACTIVE_STATUS);
            f.Add(this.ACH_PAYEEID);
            f.Add(this.APPROVED_FOR_PMT);
            //f.Add(this.DRV_VEHICLE_TYPE);

            //Reminder: string.join will use empty string for null values in the array
            return String.Join("||",f);
        }

        public PaymentMethod PreferredPaymentMethod
        {
            get
            {
                return (PaymentMethod) Enum.Parse(typeof(PaymentMethod), (this.PREFERRED_PMT_METHOD ?? "NotSet"));
            }
            set
            {
                this.PREFERRED_PMT_METHOD = value.ToString();
            }

        }
    }

    public enum PaymentMethod { NotSet=0, Check, ACH}
}
