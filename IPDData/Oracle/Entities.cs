﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
using System.Data.Entity.Infrastructure;
namespace IPD.Data.Oracle
{
    public partial class Entities
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //throw new UnintentionalCodeFirstException();
            //modelBuilder.Entity<ORD>().Property(x => x.ORD_ID).HasColumnType("Raw");
            //modelBuilder.Entity<ORD>().Property<Guid>()

            modelBuilder.Conventions.Add(new Raw16ToRaw12());
                

        }
    }

    public class Raw16ToRaw12 : System.Data.Entity.ModelConfiguration.Conventions.Convention
    {
        public Raw16ToRaw12()
        {
            this.Properties<Guid>()
                .Where(p => p.Name == "ORD_ID")
                .Configure(p => p.HasColumnType("raw(12)"));
        }
    }
}
