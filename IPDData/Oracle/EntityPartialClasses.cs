﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Oracle
{
    public partial class DRIVER
    {
        public string DRV_NAME_Display { get { return DRV_NAME.Replace(",", " "); } }
        public string FirstName { 
            get
            {
                if (String.IsNullOrEmpty(DRV_NAME))
                    return null;

                var nameParts = DRV_NAME.Split(",".ToCharArray(), StringSplitOptions.None);
                return nameParts[0];
            }
            set
            {
                if (String.IsNullOrEmpty(DRV_NAME) || DRV_NAME.IndexOf(",") < 0)
                    DRV_NAME = value + ",";
                else
                {
                    var nameParts = DRV_NAME.Split(",".ToCharArray(), StringSplitOptions.None);
                    nameParts[0] = value;
                    DRV_NAME = String.Join(",", nameParts);
                }
            }
        }
        public string LastName
        {
            get
            {
                if (String.IsNullOrEmpty(DRV_NAME) || DRV_NAME.Trim().IndexOf(",") == -1)
                    return null;
                else
                {
                    var nameParts = DRV_NAME.Split(",".ToCharArray(), StringSplitOptions.None);
                    return nameParts[1];
                }
            }

            set
            {
                if (String.IsNullOrEmpty(DRV_NAME))
                    DRV_NAME = "," + value;
                else if (DRV_NAME.IndexOf(",") < 0)
                    //assume existing value is first name
                    DRV_NAME = DRV_NAME.Trim() + "," + value;
                else
                {
                    var nameParts = DRV_NAME.Split(",".ToCharArray(), StringSplitOptions.None);
                    nameParts[1] = value;
                    DRV_NAME = String.Join(",", nameParts);
                }
            }
        }
    }
}
