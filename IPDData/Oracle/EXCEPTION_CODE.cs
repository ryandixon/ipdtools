//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IPD.Data.Oracle
{
    using System;
    using System.Collections.Generic;
    
    public partial class EXCEPTION_CODE
    {
        public System.Guid EXPCODE_ID { get; set; }
        public string EXPCODE_TYPE { get; set; }
        public System.Guid EXPCODE_CMP_ID { get; set; }
        public Nullable<System.Guid> EXPCODE_BRC_ID { get; set; }
        public Nullable<System.Guid> EXPCODE_CUS_ID { get; set; }
        public string EXPCODE_MASTER_CODE { get; set; }
        public string EXPCODE_CODE { get; set; }
        public string EXPCODE_CUSTOMER_CODE { get; set; }
        public string EXPCODE_DESCRIPTION { get; set; }
        public string EXPCODE_TRIGGER_CUSTOMER_ALERT { get; set; }
        public string EXPCODE_TRIGGER_BRANCH_ALERT { get; set; }
        public string EXPCODE_ACTIVE_STATUS { get; set; }
    }
}
