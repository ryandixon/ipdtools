﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace IPD.Data.Mongo
{

    public class GpsData : IPDMongoDocumentBase
    {
        [BsonElement("strGpsDrvId")]
        public string strGpsDrvId { get; set; }
        [BsonIgnore]
        public Guid? DriverId
        {
            get
            {
                return !String.IsNullOrEmpty(strGpsDrvId) ? new Guid(strGpsDrvId).ToOracleGuid()
                    : strGpsDrvId == null ? (Guid?)null
                    : Guid.Empty;
            }
            set { strGpsDrvId = value.ToString(); }
        }

        [BsonElement("gps")]
        public GPSCoordinates GPSCoordinates { get; set; }

        [BsonElement("datGpsTimeStamp")]
        [BsonDateTimeOptions(Kind=DateTimeKind.Utc)]
        [Display(Name = "Server Time Stamp")]        
        public DateTime TimeStamp { get; set; }

        [BsonElement("strDrvStatus")]
        public string Status { get; set; }
    }
}
