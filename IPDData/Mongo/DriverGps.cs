﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace IPD.Data.Mongo
{
    
    public class DriverGps: IPDMongoDocumentBase
    {

        [BsonElement("strDrvGPSDrvId")]
        public string strDrvGPSDrvId { get; set; }
        [BsonIgnore]
        public Guid? DriverId
        {
            get
            {
                return !String.IsNullOrEmpty(strDrvGPSDrvId) ? new Guid(strDrvGPSDrvId).ToOracleGuid()
                    : strDrvGPSDrvId == null ? (Guid?)null
                    : Guid.Empty;
            }
            set { strDrvGPSDrvId = value.ToString(); }
        }

        private string mobileNumber;
        [BsonElement("strDrvAccessCode")]
        [DisplayFormat(DataFormatString = "{0:(###)###-####}", ApplyFormatInEditMode = true, NullDisplayText = "Enter Mobile number")]
        [Display(Name = "Mobile Number")]
        public string MobileNumber 
        { 
            get { return mobileNumber; }
            set 
            {
                mobileNumber = System.Text.RegularExpressions.Regex.Replace(value, "[ ()-.]", "");
            }
        }

        [BsonElement("gps")]
        public GPSCoordinates GPSCoordinates { get; set; }

        [BsonElement("strDrvStatus")]
        public string Status { get; set; }

        [BsonElement("datDrvGPSTimestamp")]
        [BsonDateTimeOptions(Kind=DateTimeKind.Utc)]
        [Display(Name = "Server Time Stamp UTC")]
        public DateTime TimeStamp { get; set; }

        [Display(Name = "Time Stamp Local")]
        public DateTime TimeStampLocal { get { return TimeStamp.ToLocalTime(); } }

    }


    public class GPSCoordinates : IPDMongoDocumentBase
    {
        [BsonElement("latitude")]
        [BsonRepresentation(BsonType.Double, AllowTruncation = true)]
        public decimal Latitude { get; set; }

        [BsonElement("longitude")]
        [BsonRepresentation(BsonType.Double, AllowTruncation=true)]
        public decimal Longitude { get; set; }
    }
}
