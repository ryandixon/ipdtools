﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace IPD.Data.Mongo
{
    
    public class Event: IPDMongoDocumentBase
    {
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)] 
        public string _id { get; set; }
        public string _class { get; set; }
        public DateTime serverTimestamp { get; set; }
        public string strEventOrdId { get; set; }

        [BsonElement("strEventOrdNumber")]
        public string OrderNumber { get; set; }

        public string strEventDriverId { get; set; }
        public string strEventStopId { get; set; }
        public string strEventType { get; set; }
        public string strEventStatus { get; set; }
        public string strEventNotes { get; set; }
        public bool bEventProcessed { get; set; }
        public DateTime datEventTime { get; set; }
        public string strDriverName { get; set; }

    }
}
