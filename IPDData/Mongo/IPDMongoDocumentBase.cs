﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Mongo
{
    public class IPDMongoDocumentBase
    {
        /// <summary>
        /// Contains document elements that have not been serialized to specific properties
        /// </summary>
        [BsonExtraElements]
        public BsonDocument AdditionalElements { get; set; }
    }
}
