﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Data.Mongo
{
    public static class IPDMongo
    {
        public static MongoDatabase GetIPDDb(string connectionString)
        {
            BsonDefaults.GuidRepresentation = GuidRepresentation.Standard;
            var mongoCxnString = connectionString; 
            var mongoClient = new MongoClient(mongoCxnString);
            var mongoUrl = new MongoUrl(mongoCxnString);
            return mongoClient.GetServer().GetDatabase(mongoUrl.DatabaseName);
        }

        public static byte[] HexStringToByteArray(string hex)
        {
            if (string.IsNullOrEmpty(hex))
                return new byte[0];

            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
    }
}
