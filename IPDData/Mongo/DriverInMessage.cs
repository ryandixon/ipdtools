﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace IPD.Data.Mongo
{
    public class DriverInMessage: IPDMongoDocumentBase
    {
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string _id { get; set; }

        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        [BsonElement("strDrvmsgId")]
        public string MessageId { get; set; }

        [BsonElement("strDrvmsgDrvId")]
        public string strDrvmsgDrvId { get; set; }
        [BsonIgnore]
        public Guid? DriverId
        {
            get
            {
                return !String.IsNullOrEmpty(strDrvmsgDrvId) ? new Guid(strDrvmsgDrvId).ToOracleGuid()
                    : strDrvmsgDrvId == null ? (Guid?)null
                    : Guid.Empty;
            }
            set { strDrvmsgDrvId = value.ToString(); }
        }

        [BsonElement("datDrvmsgCreateTime")]
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        [Display(Name = "CreateTime")]
        public DateTime CreateTime { get; set; }

        [BsonElement("strDrvmsgType")]
        public string Type { get; set; }

        [BsonElement("strDrvmsgStatus")]
        public string Status { get; set; }

        [BsonElement("DrvmsgContent")]
        public string Content { get; set; }

        [BsonElement("datDrvmsgReceivedTime")]
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        [Display(Name = "ReceivedTime")]
        public DateTime ReceivedTime { get; set; }

        [BsonElement("strDrvmsgReadBy")]
        public string ReadBy { get; set; }

    }
}
