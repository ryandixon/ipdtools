﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace IPD.Data.Mongo
{
    public class Order: IPDMongoDocumentBase
    {
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string _id { get; set; }

        [BsonElement("strOrdId")]
        public string strOrdId { get; set; }

        private string custId;
        [BsonElement("strOrdCusId")]
        public string strOrdCustId 
        {
            get { return custId;}
            set { 
                custId = value;
                CustomerId = new Guid(custId).ToOracleGuid();
            }
        }

        /// <summary>
        /// IMPORTANT: This is the Customer ID Guid converted to Oracle Format!!
        /// </summary>
        public Guid CustomerId { get; set; }

        /* Why not map directly to a Guid using a property like this?
           
              [BsonElement("strOrdDriverId")]        
              [BsonRepresentation(MongoDB.Bson.BsonType.String)]
              public Guid? DriverId {get; set;}
          
           ...because there are null values in the document (i.e. documents with a strOrderDriverId property but without a value specified) which throws a "Unrecognized Guid Format" exception on 
           deserialization. There must/should be some mapping arraingement around this but haven't found it and I'm done looking (for now)!
         */
        [BsonElement("strOrdDriverId")]        
        public string strOrdDriverId { get; set; }        
        [BsonIgnore]
        public Guid? DriverId 
        { 
            get 
            { 
                return !String.IsNullOrEmpty(strOrdDriverId) ? new Guid(strOrdDriverId).ToOracleGuid()
                    : strOrdDriverId == null ? (Guid?) null 
                    : Guid.Empty;
            } 
            set { strOrdDriverId = value.ToString();}
        }
        /* end comment */

        [BsonElement("strOrdNumber")]
        public string OrderNumber { get; set; }

        [BsonElement("datOrdCreateTime")]
        public DateTime CreateTime { get; set; }

        [BsonElement("ord_complete_time")]
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime CompletionTime { get; set; }

        [BsonElement("strOrdStatus")]
        public string Status { get; set; }

        [BsonElement("listStops")]
        public List<Stop> Stops { get; set; }

        [BsonIgnore]
        public string CustomerName { get; set; }

        [BsonIgnore]
        public string DriverName { get; set; }

        [BsonIgnore]
        public string DriverMobileNumber { get; set; }

        [BsonIgnore]
        public Stop FirstStop { get { return Stops == null ? (Stop) null : Stops.FirstOrDefault(); } }

        [BsonIgnore]
        public Stop LastStop { get { return Stops == null ? (Stop)null : Stops.LastOrDefault(); } }

        [BsonIgnore]
        public Event MostRecentEvent { get; set; }
    }

    public class Stop: IPDMongoDocumentBase
    {
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string _id { get; set; }

        [BsonElement("strStpStatus")]
        public string Status { get; set; }

        [BsonElement("strStpSeqNumber")]
        [BsonRepresentation(MongoDB.Bson.BsonType.String)]
        public int SequenceNumber { get; set; }

        [BsonElement("strStpName")]
        public string Name { get; set; }

        [BsonElement("fullAddress")]
        public string FullAddress { get; set; }

        [BsonElement("strStpStreet")]
        public string StreetAddress { get; set; }

        [BsonElement("strStpSuite")]
        public string Suite { get; set; }

        [BsonElement("strStpCity")]
        public string City { get; set; }

        [BsonElement("strStpState")]
        public string State { get; set; }

        [BsonElement("strStpZip")]
        public string ZipCode { get; set; }

    }
}
