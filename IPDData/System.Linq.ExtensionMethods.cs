﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Linq
{
    public static class ExtensionMethods
    {
        public static string ToOracleString(this Guid guid)
        {
            return guid.ToString().ToUpper()
                .Replace("-", "")
                .TrimStart("{".ToCharArray())
                .TrimEnd("}".ToCharArray());
        }

        public static Guid ToOracleGuid(this Guid guid)
        {            
            var rtn = new Guid(BitConverter.ToString(guid.ToByteArray()).Replace("-", ""));            
            return rtn;
        }

        public static Guid ToNETGuid(this Guid guid)
        {
            var text = guid.ToOracleString();
            // Not the most efficient code in the world, but
            // it works...
            byte[] bytes = new byte[text.Length / 2];
            for (int i = 0; i < bytes.Length; i++)
            {
                bytes[i] = Convert.ToByte(text.Substring(i * 2, 2), 16);
            }
            return new Guid(bytes);
        }
    }
}
