﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data
{
    public interface IUpdateHistory
    {
        //[DateTimeKind(DateTimeKind.Utc)]
        DateTime? LastUpdatedTimeUTC { get; set; }
        string LastUpdatedBy { get; set; }
    }
}
