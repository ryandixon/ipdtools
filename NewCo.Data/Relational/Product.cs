﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    /*Examples 
     *Product: 
     *  Name: OriginatorFee, 
     *  RateCostBasis: ServiceProviderOfferAmount,
     *  Rate: .03,  
     *  MinimumCost: $1.50
     *  
     *Product: 
     *  Name: SerivceProviderFee, 
     *  RateCostBasis: ServiceProviderOfferAmount,
     *  Rate: .03,  
     *  MinimumCost: $1.50
     * 
     *Product: 
     *  Name: OriginatorFee, 
     *  RateCostBasis: DeclaredValue,
     *  Rate: .1,  
     *  MinimumCost: $2.00
     *  
     */
    /// <summary>
    /// Represents a product sold by IPD. Ex: transaction fee, driver insurace, cargo insurance, etc. 
    /// </summary>
    public class Product : ICreateHistory, IUpdateHistory
    {
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// A unique product code (that is easier to read than a GUID!)
        /// </summary>
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        /// <summary>
        /// The value to which the cost rate should be applied. 
        /// </summary>
        [Required]
        public RateCostBasis RateCostsBasis { get; set; }

        /// <summary>
        /// When not null, this value is is multiplied by the value specified in the RateCostsBasis property to determine the product's cost
        /// </summary>
        public decimal? RateCost { get; set; }

        /// <summary>
        /// When RateCost is specified, this is the minimum value that should be the cost of the item
        /// </summary>
        public decimal? MinimumCost { get; set; }

        public decimal? FixedCost { get; set; }

        [DateTimeKind(DateTimeKind.Utc)] 
        public DateTime CreateTimeUTC { get; set; }
        public string CreatedBy { get; set; }

        [DateTimeKind(DateTimeKind.Utc)] 
        public DateTime? LastUpdatedTimeUTC { get; set; }
        public string LastUpdatedBy { get; set; }

    }

    public enum RateCostBasis { 
        ServiceProviderOfferAmount=1, 
        GrossOriginatorAmountDue=2, //Excludes rate based items
        DeclaredValue=3, //Order Declared Value of goods
        StatementGross=4, //The total amount of the invoice or statement prior to other discounts and deductions
        StatementNet=5 //The net amount of the invoice or statement after other discounts and deductions
    }
}
