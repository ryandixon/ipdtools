﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    /// <summary>
    /// Holds a recuring route definition. Info herein will be used to create a separate order for each execution of the route.
    /// </summary>
    public class Route
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public int BusinessEntityId { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
