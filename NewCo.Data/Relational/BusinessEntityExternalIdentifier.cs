﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    /// <summary>
    /// Stores an identifier of a business entity provided by another entity. For example, an agent may have an account number it uses to refer to one of 
    ///     it's customers. That account number could be stored here.
    /// </summary>
    public class BusinessEntityExternalIdentifier
    {
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// The Business Entity that provided or created the Identifier
        /// </summary>
        [Required]
        public int BusinessEntityIdSource { get; set; }

        /// <summary>
        /// The Business Entity that the Indentifier refers to
        /// </summary>
        [Required]
        public int BusinessEntityIdTarget { get; set; }

        /// <summary>
        /// The identifier used to refer to the target BusinessEntity 
        /// </summary>
        [Required]
        public string Identifier { get; set; }

        /// <summary>
        /// The type of identifier as specified by the BE Source
        /// </summary>        
        public string IdentifierType { get; set; }
    }
}
