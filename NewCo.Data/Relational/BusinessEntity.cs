﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    /// <summary>
    /// Any contractor or business that we might pay or receive money from.
    /// </summary>
    public class BusinessEntity : ICreateHistory, IUpdateHistory
    {
        //public Guid Id { get; set; }
        [Key]
        public int Id { get; set; }
        public int? ParentBusinessEntityId { get; set; }
        
        [Required]
        public string OrganizationName { get; set; }

        public Guid? PrimaryContactId { get; set; }
        public Contact PrimaryContact { get; set; }

        public Guid? BillingAddressId { get; set; }
        public Address BillingAddress { get; set; }

        public Guid? PhysicalAddressId { get; set; }
        public Address PhysicalAddress { get; set; }

        public Guid? IPDAccountId { get; set; }

        //This should be handled in an accounting system!!
        //public bool ApprovedForPayment { get; set; }
        //public string PreferredPaymentMethod { get; set; }

        //public bool ApprovedForCollection { get; set; }
        //public string PreferredCollectionMethod { get; set; }

        [DateTimeKind(DateTimeKind.Utc)] 
        public DateTime CreateTimeUTC { get; set; }
        public string CreatedBy { get; set; }

        [DateTimeKind(DateTimeKind.Utc)] 
        public DateTime? LastUpdatedTimeUTC { get; set; }
        public string LastUpdatedBy { get; set; }
    }

}
