﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class ContactMethod
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public Guid ContactId { get; set; }

        [Required]
        public ContactMethodType Type { get; set; }

        [Required]
        public ContactMethodSubType SubType { get; set; }

        /// <summary>
        /// The phone number, emaill address, etc...
        /// </summary>
        public string ContactPoint { get; set; }
        /// <summary>
        /// If the method is an address, this value points to the address record
        /// </summary>
        public Guid? AddressId { get; set; }
        
        public bool IsPreferredOfType { get; set; }

        public bool ConsentToReceiveTextMessage { get; set; }
    }

    public enum ContactMethodType { EMail, Phone, Address}
    public enum ContactMethodSubType { Mobile, Work, Home, Other}
}
