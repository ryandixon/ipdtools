﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class ServiceProvider : ICreateHistory, IUpdateHistory
    {
        public int Id { get; set; }
        /// <summary>
        /// Driver number
        /// </summary>
        public string ProviderNumber { get; set; }

        [Required]
        public int BusinessEntityId { get; set; }

        /// <summary>
        /// The mobile number associated with the driver's account
        /// </summary>
        [Required]
        public string MobileNumber { get; set; }

        /// <summary>
        /// A unique invitation code valid for a specific IC
        /// </summary>
        public string InvitationCode { get; set; }

        [DateTimeKind(DateTimeKind.Utc)]
        public DateTime CreateTimeUTC { get; set; }
        public string CreatedBy { get; set; }

        [DateTimeKind(DateTimeKind.Utc)]
        public DateTime? LastUpdatedTimeUTC { get; set; }
        public string LastUpdatedBy { get; set; }

    }
}
