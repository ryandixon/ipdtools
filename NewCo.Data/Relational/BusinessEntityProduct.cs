﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    /// <summary>
    /// Holds a business entites special pricing for a specific product. When a record exists, the values here override the values from the Product Table
    /// </summary>
    public class BusinessEntityProduct : ICreateHistory, IUpdateHistory
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public int BusinessEntityId { get; set; }
        [Required]
        public Guid ProductId { get; set; }

        //Holds any comments or remarks concerning the discount - e.g. why it was given
        public string Notes { get; set; }

        /// <summary>
        /// When not null, this value is is multiplied by the value specified in the Product's RateCostsBasis property to determine the product's cost
        /// </summary>
        public decimal? RateCost { get; set; }

        /// <summary>
        /// When RateCost is specified, this is the minimum value that should be the cost of the item
        /// </summary>
        public decimal? MinimumCost { get; set; }

        public decimal? FixedCost { get; set; }

        /// <summary>
        /// The date when the special pricing becomes effetive
        /// </summary>
        [Required]
        public DateTime EffectiveDate { get; set; }

        /// <summary>
        /// The date when the special pricing terminates
        /// </summary>
        public DateTime? EffectiveEndDate { get; set; }

        [DateTimeKind(DateTimeKind.Utc)] 
        public DateTime CreateTimeUTC { get; set; }
        public string CreatedBy { get; set; }
        
        [DateTimeKind(DateTimeKind.Utc)] 
        public DateTime? LastUpdatedTimeUTC { get; set; }
        public string LastUpdatedBy { get; set; }
    }
}
