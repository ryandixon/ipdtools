﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class Address : ICreateHistory, IUpdateHistory
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }

        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }

        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }

        public int TimeZone { get; set; }

        [DateTimeKind(DateTimeKind.Utc)]
        public DateTime CreateTimeUTC { get; set; }
        public string CreatedBy { get; set; }

        [DateTimeKind(DateTimeKind.Utc)]
        public DateTime? LastUpdatedTimeUTC { get; set; }
        public string LastUpdatedBy { get; set; }

    }
}
