﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace NewCo.Data.Relational
{
    public class NewCoRelationalInitializer : System.Data.Entity. DropCreateDatabaseIfModelChanges<NewCoRelationalContext>
    {
        protected override void Seed(NewCoRelationalContext context)
        {
            context.BusinessEntities.Add(new BusinessEntity 
                { 
                    Id = 40000, 
                    OrganizationName="Order Originator, Inc." ,
                });
        }
    }
}
