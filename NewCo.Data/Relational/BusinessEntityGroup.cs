﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    /// <summary>
    /// A group of entites as defined by another BE. Designed to allow shippers to put drivers into groups of their own making but, could conceivable be used by drivers to put order originators into groups as well
    /// </summary>
    public class BusinessEntityGroup : ICreateHistory, IUpdateHistory
    {
        public Guid Id { get; set; }

        /// <summary>
        /// The Business Entity defining this group
        /// </summary>        
        public int BusinessEntityId { get; set; }

        public BusinessEntityGroupType? Type { get; set; }

        [Required]
        public string Name { get; set; }

        [DateTimeKind(DateTimeKind.Utc)]
        public DateTime CreateTimeUTC { get; set; }
        public string CreatedBy { get; set; }

        [DateTimeKind(DateTimeKind.Utc)]
        public DateTime? LastUpdatedTimeUTC { get; set; }
        public string LastUpdatedBy { get; set; }
    }

    public enum BusinessEntityGroupType { Approved, Blocked, Custom}


}
