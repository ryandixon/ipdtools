﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Infrastructure;
using NewCo.Data;
namespace NewCo.Data.Relational
{
    public class NewCoRelationalContext: DbContext
    {
        public NewCoRelationalContext(): base("NewCoRelationalContext")
        {
            ((IObjectContextAdapter)this).ObjectContext.ObjectMaterialized +=
                (sender, e) => DateTimeKindAttribute.Apply(e.Entity);
        }

        public override int SaveChanges()
        {
            var utcNowAuditDate = DateTime.UtcNow;

            //Set audit info 
            var creates = from e in ChangeTracker.Entries<ICreateHistory>().AsQueryable()
                          where e.State == EntityState.Added
                          select e;
            if (creates != null)
                foreach (DbEntityEntry<ICreateHistory> dbEntityEntry in creates)
                        dbEntityEntry.Entity.CreateTimeUTC = utcNowAuditDate;

            var updates = from e in ChangeTracker.Entries<IUpdateHistory>().AsQueryable()
                          where e.State == EntityState.Modified
                              select e;
            if (updates != null)
                foreach (DbEntityEntry<IUpdateHistory> dbEntityEntry in updates)
                        dbEntityEntry.Entity.LastUpdatedTimeUTC = utcNowAuditDate;
            
            return base.SaveChanges();
        }

        public DbSet<Accessorial> Accessorials { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Agent> Agents { get; set; }
        public DbSet<BusinessEntity> BusinessEntities { get; set; }
        public DbSet<BusinessEntityGroup> BusinessEntityGroups { get; set; }
        public DbSet<BusinessEntityGroupMembership> BusinessEntityGroupMemberships { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<ExternalOrderId> ExternalOrderIds { get; set; }
        public DbSet<Login> Logins { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderAccessorial> OrderAccessorials { get; set; }
        public DbSet<OrderProduct> OrderProducts { get; set; }
        public DbSet<OrderSecondaryParty> OrderSecondaryParties { get; set; }
        public DbSet<PerformanceRating> PerformanceRatings { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Route> Routes { get; set; }
        public DbSet<ServiceProvider> ServiceProviders { get; set; }
        public DbSet<Stop> Stops { get; set; }
        public DbSet<StopTransportPiece> StopTransportPieces { get; set; }
        public DbSet<TransportPiece> TransportPieces { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //base.OnModelCreating(modelBuilder);
        }
    }
}
