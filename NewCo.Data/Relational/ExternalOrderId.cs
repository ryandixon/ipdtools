﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class ExternalOrderId : ICreateHistory, IUpdateHistory
    {
        public Guid Id { get; set; }

        [Required]
        public Guid OrderId { get; set; }
        [Required]
        public ExternalOrderIdSource Source { get; set; }

        /// <summary>
        /// The business entity id of the source of the external Id (optional)
        /// </summary>
        public int? BusinessEntityId { get; set; }

        /// <summary>
        /// A descriptor of the External Id. Generally provided by the externally entity - e.g. Case Number, COPS Order Id, Agent's Customer's Account Id
        /// </summary>
        [Required]
        public string IdType { get; set; }

        /// <summary>
        /// The value of the external ID
        /// </summary>
        [Required]
        public string ExternalId { get; set; }

        [DateTimeKind(DateTimeKind.Utc)]
        public DateTime CreateTimeUTC { get; set; }
        public string CreatedBy { get; set; }

        [DateTimeKind(DateTimeKind.Utc)]
        public DateTime? LastUpdatedTimeUTC { get; set; }
        public string LastUpdatedBy { get; set; }
    }

    public enum ExternalOrderIdSource { Originator=1, SecondaryParty=2 }
}
