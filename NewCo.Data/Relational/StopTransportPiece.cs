﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class StopTransportPiece
    {
        [Key]
        [Column(Order=1)]
        public Guid StopId { get; set; }
        [Key]
        [Column(Order = 2)]
        public Guid TransportPieceId { get; set; }
        public int ScheduledNumberPieces { get; set; }
        public int ActualNumberPieces { get; set; }
        public PieceAction ScheduledAction { get; set; }
        public PieceAction ActualAction { get; set; }
    }
    public enum PieceAction {Pickup, Deliver}
}
