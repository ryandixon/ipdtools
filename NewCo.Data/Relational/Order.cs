﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NewCo.Data;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NewCo.Data.Relational
{
    public class Order: ICreateHistory, IUpdateHistory
    {
        public Order()
        {
            Id = Guid.NewGuid();
            Status = OrderStatus.New;
            CollectionStatus = OrderCollectionStatus.Unbilled;
            PaymentStatus = OrderPaymentStatus.Unpaid;
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        /// <summary>
        /// A system assigned unique string identifying this order
        /// </summary>
        
        //[DatabaseGenerated(DatabaseGeneratedOption.Computed)]         //Need a trigger or the like to set the value of this column!!!
        public string OrderNumber { get; set; }

        /// <summary>
        /// When not null, the guid of the route this order was created from.
        /// </summary>
        public Guid? RouteId { get; set; }

        /// <summary>
        /// The id (guid) of the BusinessEntity who originated this order. This is the entity from whom funds will be collected to for the completion of this order.
        /// </summary>
        [Required]
        public int OriginatorId { get; set; }

        /// <summary>
        /// The id (guid) of the BusinessEntity who is the service provider that will complete this order. This is the entity that will be paid.
        /// </summary>
        public int? ServiceProviderId { get; set; }

        [Required]
        public OrderStatus Status { get; set; }

        [Required]
        public OrderPaymentStatus PaymentStatus { get; set; }
        [Required]
        public OrderCollectionStatus CollectionStatus { get; set; }

        /// <summary>
        /// The stated driver settlement amount. I.e. the amount agreed to by the order orginator and service provider for completion of the order
        /// </summary>
        public decimal? ServiceProviderOfferAmount { get; set; }

        [Required]
        public int OfferExpirationSeconds { get; set; }
        public decimal DeclaredValue { get; set; }
        public bool IncludesDangerousGoods { get; set; }

        /// <summary>
        /// When true, this order should automatically be assigned to the first service provider to bid on it.
        /// </summary>
        public bool AutoAssignToFirstBidder { get; set; }

        [DateTimeKind(DateTimeKind.Utc)] 
        public DateTime CreateTimeUTC { get; set; }
        public string CreatedBy { get; set; }

        [DateTimeKind(DateTimeKind.Utc)] 
        public DateTime? LastUpdatedTimeUTC { get; set; }
        public string LastUpdatedBy { get; set; }

    }

    public enum OrderStatus { New = 10, Offered = 20, Assigned = 30, Started = 40, Complete = 50, Canceled = 60, Aborted = 70, Closed = 80 }

    public enum OrderPaymentStatus { Unpaid = 10, PaymentPending = 20, PartiallyPaid = 30, Complete = 40 }

    public enum OrderCollectionStatus { Unbilled = 10, Billed = 20, PartiallyCollected = 30, Complete = 40 }
}
