﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    /// <summary>
    /// A business entity's membership in a group defined by another business entity.  Multiple membership records for the same Member are permitted so long as they do not overlap timeperiods!!
    /// </summary>
    public class BusinessEntityGroupMembership : ICreateHistory, IUpdateHistory
    {
        [Key]
        [Column(Order=1)]
        public Guid BusinessEntityGroupId { get; set; }

        /// <summary>
        /// The Business Entity that is a member of the BusinessEntityGroup
        /// </summary>
        [Key]
        [Column(Order = 2)]
        public Guid MemberId { get; set; }

        /// <summary>
        /// The begin datetime of the membership.
        /// </summary>
        [Required]
        public DateTime MembershipBeginTime { get; set; }


        /// <summary>
        /// When set, the datetime when the members inclusion in the group expires.
        /// </summary>
        public DateTime? MembershipEndTime { get; set; }

        [DateTimeKind(DateTimeKind.Utc)]
        public DateTime CreateTimeUTC { get; set; }
        public string CreatedBy { get; set; }

        [DateTimeKind(DateTimeKind.Utc)]
        public DateTime? LastUpdatedTimeUTC { get; set; }
        public string LastUpdatedBy { get; set; }

    }}
