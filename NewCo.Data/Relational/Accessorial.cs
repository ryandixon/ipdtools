﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class Accessorial
    {
        /// <summary>
        /// A code stipulating the accessorial
        /// </summary>
        public int Id { get; set; }

        [Required]
        public string Description { get; set; }
    }
}
