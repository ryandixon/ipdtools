﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    /// <summary>
    /// This contians a product associated with an order
    /// </summary>
    public class OrderProduct : ICreateHistory, IUpdateHistory
    {
        [Key]
        [Column(Order = 1)]
        public Guid OrderId { get; set; }
        [Key]
        [Column(Order = 2)]
        public Guid ProductId { get; set; }

        /// <summary>
        /// Id of special pricing that is applied to this product
        /// </summary>
        public Guid? BusinessEntityProductId { get; set; }

        /// <summary>
        /// The business Entity Id of the party purchasing this product. i.e. the originator or the service provider. 
        /// </summary>
        [Required]
        public Guid PurchaserId { get; set; }

        [Required]
        public decimal UnitPrice { get; set; }

        [Required]
        public int Quantity { get; set; }

        [DateTimeKind(DateTimeKind.Utc)] 
        public DateTime CreateTimeUTC { get; set; }
        public string CreatedBy { get; set; }

        [DateTimeKind(DateTimeKind.Utc)] 
        public DateTime? LastUpdatedTimeUTC { get; set; }
        public string LastUpdatedBy { get; set; }
    }
}
