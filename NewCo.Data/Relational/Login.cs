﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class Login : ICreateHistory, IUpdateHistory
    {
        public Guid Id { get; set; }
        /// <summary>
        /// The business entity associated with this login - Agent, Shipper, IPD User, etc.
        /// </summary>
        public int BusinessEntityId { get; set; }
        public string UserName { get; set; }
        public Guid ContactId { get; set; }

        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public string PasswordQuestion { get; set; }
        public string PasswordAnswer { get; set; }
        public bool IsLocked { get; set; }

        public int FailedPasswordAttemptCount { get; set; }
        public int? LastFailedPasswordAttemptTime { get; set; }

        [DateTimeKind(DateTimeKind.Utc)] 
        public DateTime? LastSuccessfulLoginTime { get; set; }

        [DateTimeKind(DateTimeKind.Utc)]
        public DateTime CreateTimeUTC { get; set; }
        public string CreatedBy { get; set; }

        [DateTimeKind(DateTimeKind.Utc)]
        public DateTime? LastUpdatedTimeUTC { get; set; }
        public string LastUpdatedBy { get; set; }

    }
}
