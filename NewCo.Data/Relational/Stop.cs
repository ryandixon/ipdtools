﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class Stop : ICreateHistory, IUpdateHistory
    {
        public Guid Id { get; set; }

        /// <summary>
        /// Id of order that this stop is included in. Allways null if RouteId is populated.
        /// </summary>
        public Guid? OrderId { get; set; }

        /// <summary>
        /// Id of predefined route. Always null if OrderId is populated.
        /// </summary>
        public Guid? RouteId { get; set; }


        public Guid? AddressId { get; set; }
        public Address Address { get; set; }
        /*
         * The address fields are duplicated here to capture an accurate snapshot of the address at the
         * the time the order was created. Subsequent changes to the address in the address table will not be reflected
         * in these fields
         */
        public string Description { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public int TimeZone { get; set; }

        
        /// <summary>
        /// The !LOCAL! "from" time that a service provider should not arrive prior to when completing a stop. Expressed as !LOCAL TIME! to the stop's location - NOT UTC!
        /// Using local time is important because the begin time is, for example, 8am even if a daylight savings time shift occurs between the stop/order creation and the actual event
        /// </summary>
        public DateTime? ScheduledBeginTime { get; set; }

        /// <summary>
        /// The !LOCAL! "to" time that a service provider should not arrive after to when completing a stop. Expressed as !LOCAL TIME! to the stop's location - NOT UTC!
        /// Using local time is important because the end time is, for example, 5pm even if a daylight savings time shift occurs between the stop/order creation and the actual event
        /// </summary>
        public DateTime? ScheduledEndTime { get; set; }
        

        /// <summary>
        /// When true, the service provider must go inside a building to complete the stop.
        /// </summary>
        public bool BuildingEntranceRequired { get; set; }

        public string ContactName { get; set; }
        public string ContactPhoneNumber { get; set; }
        public string ContactEmail { get; set; }
        public string Note { get; set; }

        public bool SignatureRequired { get; set; }
        public string NameOfSignor { get; set; }
        public Guid? SignatureImageId { get; set; }
        [DateTimeKind(DateTimeKind.Utc)] 
        public DateTime? StopCompletionTime { get; set; }
        public string ServiceProviderNote { get; set; }

        [DateTimeKind(DateTimeKind.Utc)] 
        public DateTime CreateTimeUTC { get; set; }
        public string CreatedBy { get; set; }

        [DateTimeKind(DateTimeKind.Utc)] 
        public DateTime? LastUpdatedTimeUTC { get; set; }
        public string LastUpdatedBy { get; set; }

    }
}
