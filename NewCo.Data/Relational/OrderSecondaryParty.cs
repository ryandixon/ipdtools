﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Relational
{
    public class OrderSecondaryParty
    {
        [Key]
        [Column(Order=1)]
        public Guid OrderId { get; set; }

        [Key]
        [Column(Order = 2)]
        public int BusinessEntityId { get; set; }

        [Required]
        public SecondaryPartyRelationship Relationship {get; set;}
    }

    public enum SecondaryPartyRelationship 
    {
        Shipper=1, 
        Receiver=2, 
        Agent=4,
        Other=8 //A party probably with ReadOnly needs to view the data. Gov't monitor prehaps?
    }
}
