﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NewCo.Data
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Converts an int64 to a short character string
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public static string ToChar32String(this long i)
        {
            return IntToChar32Encoder.Encode(i);
        }

        /// <summary>
        /// Converts a string containing a 32char encoded value to a Int64 (long).
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static long ToLong(this string s)
        {
            return IntToChar32Encoder.Decode(s);
        }

        //public void ExecuteNonQueryBatch(this System.Data.Entity.DbContext context, string sqlStatements)
        //{
        //    if (sqlStatements == null) throw new ArgumentNullException("sqlStatements");
        //    if (sqlStatements == null) throw new ArgumentNullException("connectionString");

        //    using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(context.Database.Connection.ConnectionString))
        //    {
        //        Regex r = new Regex(@"^(\s|\t)*go(\s\t)?.*", RegexOptions.Multiline | RegexOptions.IgnoreCase);

        //        foreach (string s in r.Split(sqlStatements))
        //        {
        //            //Skip empty statements, in case of a GO and trailing blanks or something
        //            string thisStatement = s.Trim();
        //            if (String.IsNullOrEmpty(thisStatement)) continue;

        //            using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(thisStatement, connection))
        //            {
        //                cmd.CommandType = System.Data.CommandType.Text;

        //                cmd.ExecuteNonQuery();
        //            }
        //        }
        //    }
        //}

    }

    /// <summary>
    /// Uses 32 characters to generate a unique code from an integer Native Bijective Function: https://gist.github.com/dgritsko/9554733
    /// </summary>
    public class IntToChar32Encoder
    {
        public static readonly char[] Alphabet = "ABCDEFGHIJKMNPQRSTUVWXYZ23456789".ToCharArray();
        public static readonly int Base = Alphabet.Length;

        public static string Encode(Int64 i)
        {
            if (i == 0) return Alphabet[0].ToString();

            var s = string.Empty;

            while (i > 0)
            {
                s += Alphabet[i % Base];
                i = i / Base;
            }

            return string.Join(string.Empty, s.Reverse());
        }

        public static Int64 Decode(string s)
        {
            Int64 i = 0;

            foreach (var c in s)
            {
                i = (i * Base) + Array.FindIndex(Alphabet, item => item == c);
            }

            return i;
        }

    }
}
