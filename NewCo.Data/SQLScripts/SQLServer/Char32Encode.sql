﻿IF EXISTS (SELECT * FROM sys.objects WHERE name = 'Char32Encode')
	DROP FUNCTION Char32Encode;
GO

CREATE FUNCTION dbo.Char32Encode(@intVal Bigint)
RETURNS varchar(256)
AS 
BEGIN
	DECLARE @stringVal varchar(256),	@BaseCount int, @returnVal varchar(256), @iTemp Bigint, @cTemp char
	DECLARE @Alphabet TABLE ([Character] char, Position tinyInt)

	INSERT INTO @Alphabet
	SELECT [Character],Position FROM Char32EncodingAlphabet with (nolock);

	select @BaseCount = count(*) FROM @Alphabet;

	if (@intVal = 0)
	BEGIN
		SELECT @returnVal = [Character] FROM @Alphabet
		WHERE Position = 0;
	END
	ELSE
	BEGIN
		SET @iTemp = @intVal;
		SET @returnVal='';

		
		While @iTemp > 0
		BEGIN
			--SET @returnVal = 'x' + CAST(@iTemp as varchar(max))

			--SELECT @returnVal = Concat([Character], @returnVal)
			SELECT @cTemp = [Character]
			FROM @Alphabet
			WHERE Position = @iTemp % @BaseCount;

			SET @returnVal = Concat(@cTemp, @returnVal);
			SET @iTemp = @iTemp / @BaseCount;
		END

	END

	return @returnVal
END;
GO
