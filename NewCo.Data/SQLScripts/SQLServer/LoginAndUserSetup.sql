﻿--These, of course, will have to be run manually in SQL server


--Create the Login on the Master
USE [master]
GO
IF NOT EXISTS 
    (SELECT name   FROM master.sys.server_principals
     WHERE name = 'NewCo')
BEGIN

	CREATE LOGIN [NewCo] WITH PASSWORD=N'M@k3ItHapp3n', 
					 DEFAULT_DATABASE=[NewCo], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
END
GO

--Create the db user
USE [NewCo]
GO
IF NOT EXISTS
    (SELECT name FROM sys.database_principals
     WHERE name = 'NewCo')
BEGIN
	CREATE USER [NewCo] FOR LOGIN [NewCo] 
END
GO

exec sp_addrolemember 'db_owner', 'NewCo';
GO