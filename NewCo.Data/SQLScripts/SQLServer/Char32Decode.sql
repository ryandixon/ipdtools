﻿
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'Char32Decode')
	DROP FUNCTION dbo.Char32Decode;
GO
--here's a comment
CREATE FUNCTION dbo.Char32Decode(@stringVal varchar(256))
RETURNS BigInt
AS 
BEGIN
	DECLARE @BaseCount int, @returnVal BigInt, @iTemp Bigint, @cTemp char, @len int, @s char

	DECLARE @Alphabet TABLE ([Character] char, Position smallint)
	INSERT INTO @Alphabet
	SELECT [Character],Position FROM Char32EncodingAlphabet with (nolock)

	select @BaseCount = count(*) FROM @Alphabet

	SET @len = len(@stringVal)
	SET @returnVal = 0
	declare @iter int
	SET @iter=1
	while (@iter < @len + 1)
	BEGIN

		SET @s = SUBSTRING(@stringVal,@iter,1);
		
		SELECT @iTemp = Position
		FROM @Alphabet
		WHERE [Character] = @s;

		SET @returnVal = (@returnVal * @BaseCount) + @iTemp;

		SET @iter = @iter + 1;
	END
	

	return @returnVal
END;
GO