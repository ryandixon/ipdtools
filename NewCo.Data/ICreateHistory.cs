﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data
{
    public interface ICreateHistory
    {
        //[DateTimeKind(DateTimeKind.Utc)]
        DateTime CreateTimeUTC { get; set; }
        string CreatedBy { get; set; }
    }

}
