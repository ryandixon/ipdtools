﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using NewCo.Data;

namespace NewCo.Data.Sequences
{
    public class SequenceContextInitializer: System.Data.Entity.DropCreateDatabaseIfModelChanges<SequenceContext>
    {
        protected override void Seed(SequenceContext context)
        {
            context.Sequence.Add(new Sequence { BusinessEntityId = 40000, LastValue = 1000, Type = SequenceType.Order });
            context.Sequence.Add(new Sequence { BusinessEntityId = 40000, LastValue = 1000, Type = SequenceType.Address });
            context.Sequence.Add(new Sequence { BusinessEntityId = 40000, LastValue = 1000, Type = SequenceType.Stop });

            #region seed char32 alphabet table and procedures
            //foreach (var c in IntToChar32Encoder.Alphabet)
            for (var i = 0; i < IntToChar32Encoder.Alphabet.Length; i++)
                context.Char32EncodingAlphabet.Add(new Char32EncodingAlphabet { Character = IntToChar32Encoder.Alphabet[i].ToString(), Position = i });

            ExecuteNonQueryBatch(context.Database.Connection.ConnectionString, SQLScripts.SQLServer.SQLServerScripts.Char32Decode);
            ExecuteNonQueryBatch(context.Database.Connection.ConnectionString, SQLScripts.SQLServer.SQLServerScripts.Char32Encode);
            #endregion

            ExecuteNonQueryBatch(context.Database.Connection.ConnectionString, SQLScripts.SQLServer.SQLServerScripts.spGetNextEncodedSequenceValue);
        }

        private void ExecuteNonQuery(string cxnString, string query)
        {
            using (SqlConnection connection = new SqlConnection( cxnString))
            {
                SqlCommand command = new SqlCommand(query, connection);
                command.Connection.Open();
                command.ExecuteNonQuery();
            }
        }

        private void ExecuteNonQueryBatch(string connectionString, string sqlStatements)
        {
            if (sqlStatements == null) throw new ArgumentNullException("sqlStatements");
            if (sqlStatements == null) throw new ArgumentNullException("connectionString");

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                Regex r = new Regex(@"^(\s|\t)*go(\s\t)?.*", RegexOptions.Multiline | RegexOptions.IgnoreCase);

                connection.Open();

                foreach (string s in r.Split(sqlStatements))
                {
                    //Skip empty statements, in case of a GO and trailing blanks or something
                    string thisStatement = s.Trim();
                    if (String.IsNullOrEmpty(thisStatement)) continue;

                    using (SqlCommand cmd = new SqlCommand(thisStatement, connection))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;

                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }
    }
}
