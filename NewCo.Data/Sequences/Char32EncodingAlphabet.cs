﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NewCo.Data.Sequences
{
    public class Char32EncodingAlphabet
    {
        //public int Id { get; set; }

        [Key]
        [Column(Order = 0)]
        public string Character { get; set; }

        public int Position { get; set; }
    }
}
