﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Infrastructure;

namespace NewCo.Data.Sequences
{
    public class SequenceContext : DbContext
    {
        public SequenceContext()
            : base("SequenceContext")
        {
            ((IObjectContextAdapter)this).ObjectContext.ObjectMaterialized +=
                (sender, e) => DateTimeKindAttribute.Apply(e.Entity);
        }

        public DbSet<Sequence> Sequence { get; set; }
        public DbSet<Char32EncodingAlphabet> Char32EncodingAlphabet { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Char32EncodingAlphabet>()
            .Property(p => p.Character)
            .HasColumnType("char");

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //base.OnModelCreating(modelBuilder);
        }
    }
}
