﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewCo.Data.Sequences
{
    public enum SequenceType { Order=0, Address=1, Stop=2 }

    public class Sequence
    {
        [Key]
        [Column(Order = 1)]
        public int BusinessEntityId { get; set; }
        [Key]
        [Column(Order = 2)]
        public SequenceType Type { get; set; }
        public long LastValue { get; set; }
    }
}
