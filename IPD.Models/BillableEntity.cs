﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Models
{
    /// <summary>
    /// Any contractor or business that we might pay or receive money from.
    /// </summary>
    public class BusinessEntity
    {
        public Guid Id { get; set; }
        public string OrganizationName { get; set; }
        public Contact PrimaryContact { get; set; }
        public Address BillingAddress { get; set; }
        public Address PhysicalAddress { get; set; }
    }

    public class Account
    {
        public Guid Id { get; set; }
        public string AccountNumber { get; set; }
        public BusinessEntity BusinessEntityId { get; set; }
        public string ExternalId { get; set; }
    }
}
