﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Models
{
    public class GpsCoordinates
    {
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
}
