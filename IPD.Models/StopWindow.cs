﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Models
{
    public class StopWindow
    {
        public DateTimeOffset ArrivalFrom {get; set;}
        public DateTimeOffset ArrivalTo { get; set; }

        public TimeZone TimeZone { get; set; }
    }
}
