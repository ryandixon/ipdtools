﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Models
{
    /// <summary>
    /// A business entity that performs services on for shippers and agents - e.g. a driver.
    /// </summary>
    public class ServiceProvider : BusinessEntity
    {

        public string ProviderNumber { get; set; }


        /// <summary>
        /// The phone number associated with their mobile app
        /// </summary>
        public string MobileNumber { get; set; }

        public Contact PrimaryContact { get; set; }

        public List<CredentialDocument> Credentials { get; set; }

    }

    public class CredentialDocument
    {
        public Guid Id { get; set; }
        public BusinessEntity BusinessEntity { get; set; }
        public enum DocumentType {AutoInsurance, OccAccInsurance, CargoInsurance, MotorVehicleRecord, DriversLicense, Other}
        public DocumentType Type {get; set;}

        public string Description { get; set; }

    }

    
}
