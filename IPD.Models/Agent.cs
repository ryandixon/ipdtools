﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Models
{
    /// <summary>
    /// An entity that plays some monetary role in the completion of the order that is not the shipper, the driver, or an employee of the entity. Usually this is a logistics company using
    /// IPD drivers to fulfill orders on behalf of their (the entity's) customer. Examples:
    /// 
    /// Courier Company
    /// Master IC?
    /// 
    /// </summary>
    public class Agent : BusinessEntity
    {

        public Guid AgentId { get; set; }
        public string AgentNumber { get; set; }


    }
}
