﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Models
{
    public class JSONEnvelope<T> where T: class
    {
        public string apiVersion { get { return "1.0.0.0"; } }

        public object data { get; set; }
        public int responseCode { get; set; }
        
        public string errorCode { get; set; }
        public string errorMessage { get; set; }
    }
}
