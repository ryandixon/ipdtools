﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Models
{
    public class Stop
    {
        public Guid Id { get; set; }
        public StopWindow StopWindow { get; set; }
        public StopLocation StopAddress {get; set;}
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
    }
}
