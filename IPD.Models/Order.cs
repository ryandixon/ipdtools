﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPD.Models
{
    public class Order
    {
        public Order() {
            Stops = new List<Stop>();
        }
        public Guid Id { get; set; }

        /// <summary>
        /// IPD Order Number, returned by API after successfull order creation.
        /// </summary>
        public string OrderNumber { get; set; }

        /// <summary>
        /// Reference id to order orginators system (Optional)
        /// </summary>
        public List<string> ExternalReferenceIds { get; set; }

        /// <summary>
        /// Reference ids to shipper's system (optional)
        /// </summary>
        public List<string> ShipperReferenceIds { get; set; }

        /// <summary>
        /// The mobile number of the driver that will complete this order.
        /// </summary>
        public string ServiceProviderMobileNumber { get; set; }

        #region IPD Entity references
        /// <summary>
        /// The party that created this order, 
        /// </summary>
        public BusinessEntity Originator { get; set; }

        /// <summary>
        /// The party that will be billed for this order
        /// </summary>
        public BusinessEntity BillableEntity { get; set; }
        public decimal BillableAmount { get; set; }


        /// <summary>
        /// The Party the will be paid for this order, usually a driver
        /// </summary>
        public BusinessEntity PayableEntity { get; set; }
        public decimal PayableAmount { get; set; }

        public List<Stop> Stops { get; set; }
        #endregion

    }
}
