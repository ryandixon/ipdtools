﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPD.Web.Controllers
{
    
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]        
        public ActionResult MyOrders()
        {
            ViewBag.Message = "Order History page.";
            var model = new Models.DriverStatementViewModel();
            model.AsOfDate = DateTime.UtcNow;
            return View("~/Views/Home/MyOrders.cshtml", model);
        }

        [HttpPost]
        public ActionResult MyOrders(Models.DriverStatementViewModel model)
        {
            var settlementGenerator = new IPD.Reports.DataGenerators.Settlement();
            using( var oracleEntities = new Data.Oracle.Entities())
            {
                var rtn = settlementGenerator.GenerateDriverStatements(oracleEntities, model.AsOfDate);
                model.DriverStatement = rtn;

            }

            return View("~/Views/Home/MyOrders.cshtml", model);
        }
    }
}