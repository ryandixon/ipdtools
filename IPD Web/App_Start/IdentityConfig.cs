﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using IPD.Web.Models;

namespace IPD.Web
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.
            return Task.FromResult(0);
        }
    }

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }

    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
            this.PasswordHasher = new SHA256PasswordHasher();
        }
        public PasswordHasher PasswordHasher { get; set; }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context) 
        {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            //manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<ApplicationUser>
            //{
            //    MessageFormat = "Your security code is {0}"
            //});
            //manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<ApplicationUser>
            //{
            //    Subject = "Security Code",
            //    BodyFormat = "Your security code is {0}"
            //});

            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = 
                    new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
        
        
        public class SHA256PasswordHasher: PasswordHasher
        {
            public override string HashPassword(string password)
            {
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(password);
                var hasher = System.Security.Cryptography.SHA256Cng.Create();
                byte[] hash = hasher.ComputeHash(bytes);
                var hashedPwd = new System.Text.StringBuilder();
                foreach (byte x in hash)
                {
                    hashedPwd.AppendFormat("{0:x2}", x);
                }
                return hashedPwd.ToString();
            }

            public override PasswordVerificationResult VerifyHashedPassword(string hashedPassword, string providedPassword)
            {
                return VerifyHashedPassword(hashedPassword, providedPassword, string.Empty);
            }

            public PasswordVerificationResult VerifyHashedPassword(string hashedPassword, string providedPassword, string salt = "")
            {
                if (salt == string.Empty)                
                    //assume wintoa's method of hashing:  sha256(sha256(password)+password)
                    return HashPassword(HashPassword(providedPassword) + providedPassword) == hashedPassword ? PasswordVerificationResult.Success : PasswordVerificationResult.Failed;
                else
                    //assume salted method of hashing
                    return HashPassword(salt + providedPassword) == hashedPassword ? PasswordVerificationResult.Success : PasswordVerificationResult.Failed;

            }
        }
    }

    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }
        
        public override Task<SignInStatus> PasswordSignInAsync(string userName, string password, bool isPersistent, bool shouldLockout)
        {
            return SignInAsync(userName, password, isPersistent, shouldLockout);
        }

        public static Task<SignInStatus> SignInAsync(string userName, string password, bool isPersistent, bool shouldLockout)
        {
            return Task.Run<SignInStatus>(() =>
            {
                var hashedPwd = password;
                hashedPwd = HashString(hashedPwd, System.Text.Encoding.UTF8);
                hashedPwd = HashString(hashedPwd + password, System.Text.Encoding.UTF8);
                using (var oracleEntities = new IPD.Data.Oracle.Entities())
                {
                    var drv = (from d in oracleEntities.DRIVERs
                               where d.DRV_MOBILE_NUMBER == userName
                                 && d.DRV_PASSWORD == hashedPwd
                               select d.DRV_ID)
                               .FirstOrDefault();
                    if (drv == null)
                        return SignInStatus.Failure;
                    else
                        return SignInStatus.Success;
                }

            }); ;
        }

        public static string HashString(string text, System.Text.Encoding encoding)
        {
            byte[] bytes = encoding.GetBytes(text);
            var hasher = System.Security.Cryptography.SHA256Cng.Create();
            //var hasher = System.Security.Cryptography.MD5.Create();
            byte[] hash = hasher.ComputeHash(bytes);
            var hashedPwd = new System.Text.StringBuilder();
            foreach (byte x in hash)
            {
                //hashString += String.Format("{0:x2}", x);
                hashedPwd.AppendFormat("{0:x2}", x);
            }
            return hashedPwd.ToString();
        }
    }
}
