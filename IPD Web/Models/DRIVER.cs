﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPD.Data.Oracle
{
    public class User : DRIVER, IUser
    {

        public string Id
        {
            get { return base.DRV_ID.ToString(); }
        }

        public string UserName
        {
            get { return base.DRV_NAME; }
            set
            {
                if (base.DRV_NAME != value)
                    base.DRV_NAME = value;
            }
        }
    }

    //public class MyUserStore<TUser>: 
    //    IUserLoginStore<TUser>, IUserClaimStore<TUser>, IUserRoleStore<TUser>, IUserPasswordStore<TUser>, IUserSecurityStampStore<TUser>, IUserStore<TUser>, IDisposable 
    //    where TUser : User
    //{
    //    private readonly Oracle.Entities oracleEntities;
    //    public MyUserStore(Oracle.Entities context)
    //    {
    //        oracleEntities = context;
    //    }

    //    public async System.Threading.Tasks.Task AddLoginAsync(TUser user, UserLoginInfo login)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public System.Threading.Tasks.Task<TUser> FindAsync(UserLoginInfo login)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public System.Threading.Tasks.Task<IList<UserLoginInfo>> GetLoginsAsync(TUser user)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public System.Threading.Tasks.Task RemoveLoginAsync(TUser user, UserLoginInfo login)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public async System.Threading.Tasks.Task CreateAsync(TUser user)
    //    {
    //        //throw new NotImplementedException();
    //        if (user.DRV_ID == null || user.DRV_ID == Guid.Empty)
    //            user.DRV_ID = Guid.NewGuid();

    //        oracleEntities.DRIVERs.Add(user);
    //        await oracleEntities.SaveChangesAsync();
    //    }

    //    public System.Threading.Tasks.Task DeleteAsync(TUser user)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public System.Threading.Tasks.Task<TUser> FindByIdAsync(string userId)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public System.Threading.Tasks.Task<TUser> FindByNameAsync(string userName)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public System.Threading.Tasks.Task UpdateAsync(TUser user)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public void Dispose()
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
}