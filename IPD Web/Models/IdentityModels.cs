﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;

namespace IPD.Web.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public Guid DRV_ID { get; set; }

        public override string Id
        {
            get
            {
                return DRV_ID.ToString();
            }
            set
            {
                if (DRV_ID == null || DRV_ID == Guid.Empty)
                    DRV_ID = new Guid(value);
            }
        }

        public override string UserName
        {
            get
            {
                return base.UserName;
            }
            set
            {
                base.UserName = value.Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "").Replace("+", "").Replace(".", "");
            }
        }

        public override System.Collections.Generic.ICollection<IdentityUserRole> Roles
        {
            get
            {
                return new System.Collections.Generic.List<IdentityUserRole>() { new IdentityUserRole() { RoleId="driver", UserId = this.Id  }};
            }
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("AuthenticationConnection", throwIfV1Schema: false)
            //: base("Entities", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("IPD");
            modelBuilder.Entity<ApplicationUser>()
                .Map(t =>
                {
                    //t.Property(p => p.PhoneNumber).HasColumnName("DRV_MOBILE_NUMBER");
                    
                    t.Property(p => p.PasswordHash).HasColumnName("DRV_PASSWORD");
                    t.Property(p => p.Email).HasColumnName("DRV_EMAIL");
                    t.Property(p => p.UserName).HasColumnName("DRV_MOBILE_NUMBER");
                    t.Property(p => p.DRV_ID).HasColumnName("DRV_ID");
                    
                    t.ToTable("DRIVER");
                })
                .Ignore(t => t.EmailConfirmed)
                .Ignore(t => t.LockoutEnabled)
                .Ignore(t => t.PhoneNumberConfirmed)

                .Ignore(t => t.Roles)
                .Ignore(t => t.Claims)
                .Ignore(t => t.Logins)
                
                ;

            modelBuilder.Ignore<IdentityUserLogin>();
            modelBuilder.Ignore<IdentityRole>();
            modelBuilder.Ignore<IdentityUserClaim>();

            //modelBuilder.Entity<IdentityUserLogin>().HasKey<string>(l => l.UserId);
            //modelBuilder.Entity<IdentityRole>().HasKey<string>(r => r.Id);
            //modelBuilder.Entity<IdentityUserRole>().HasKey(r => new { r.RoleId, r.UserId });

            
        }
    }
}