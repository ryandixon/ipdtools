﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using IPD.Reports.DataSetTypes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
namespace IPD.Web.Models
{
    public class DriverStatementViewModel
    {
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true, NullDisplayText="Please select...")]
        [Display(Name="As Of Date")]
        public DateTime AsOfDate { get; set; }
        public List<DriverStatement> DriverStatement {get; set;}
    }
}