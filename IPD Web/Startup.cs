﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IPD.Web.Startup))]
namespace IPD.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
